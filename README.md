# README #

### Restart ###

Restart server with `supervisorctl restart mecusto`

### Localization ###

    * ./manage.py makemessages -l ru
    * ./manage.py makemessages -l et
    
### Configuration Files ###

The configuration files is located at `conf` directory.

- gunicorn:
    * mecusto.py **is** *gunicorn configuration file*
- nginx
    * mecusto.conf **is** *nginx configuration file*
- supervisor
    * mecusto.ini **is** *supervisor configuration file for site*
    * memcached.ini **is** *supervisor configuration file for memcached*


### Logs ###

Logs directory is **`projectdirectory`/logs/`dirname`**. The `dirname` can be obe of the following:

- gunicorn:
- nginx
- supervisor
- memcached
    
There are **access.log** and **errors.log** in each of them.
    
### Pipeline ###

For pipeline we need these packages to be installed:

- nodejs
- yuglify
- coffeescript
- lesscss

With this ones installed globally all should be fine.

Basket index.html full HTML is here [index.html](https://bitbucket.org/Julianka/mecusto-project/src/bde18c3b8b07a99b6fb23afa923018f1f881b99a/mecusto/templates/basket/index.html?at=master)
