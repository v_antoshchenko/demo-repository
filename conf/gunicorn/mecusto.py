# gunicorn can reload source files while this is set to True
# you, probably, want to set it to False if on production
reload = False

bind = 'unix:/tmp/mecusto.sock'
pidfile = '/tmp/mecusto.pid'
daemon = False
keepalive = 2

import os
import multiprocessing

workers = 2 * multiprocessing.cpu_count()
# threads = 3 * multiprocessing.cpu_count() # do not work

# logging
PROJDIR = os.path.abspath(os.path.join(os.path.basename(__file__), os.pardir, os.pardir))
accesslog = '/var/log/gunicorn_mecusto/access.log'
errorlog = '/var/log/gunicorn_mecusto/error.log'

chdir = os.path.abspath(os.path.join(PROJDIR, 'mecusto'))
pythonpath = chdir
