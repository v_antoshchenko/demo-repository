class Basket

  storage = Store

  constructor: ->
    @products = JSON.parse(storage.get("basket") or "[]")
    @promoCode = storage.get("basketPromoCode") or null

    @update()

  # NOTE: for api control
  makeTimestamp: ->
    if not Date.now
      Date.now = ->
        Date.getTime()
    Date.now()

  getAllProductsCount: ->
    if @products.length > 0
      return _.reduce(_.map(@products, (prod) -> prod.quantity), (prev, curr) -> prev + curr)
    return @products.length

  getTotalPrice: ->
    if @products.length > 0
      return _.reduce(_.map(@products, (prod) -> p = Basket.getProductInstanceFromDataObject(prod);p.getFullPrice()), (prev, curr) -> prev + curr)
    return 0

  getTotalWeight: ->
    if @products.length > 0
      return _.reduce(_.map(@products, (prod) -> p = Basket.getProductInstanceFromDataObject(prod);p.getFullWeight()), (prev, curr) -> prev + curr)
    return 0

  getTransportPrice: ->
    $('select[name=city] option:selected').data 'price'

  update: ->
    @_updateStore()
    @_updateHTML()

  # TODO: prefer to use this method
  @getProductInstanceFromDataObject: (dataObject) ->
    product = new window["#{_.first(dataObject.productType).toUpperCase()}#{_.rest(dataObject.productType).join ''}"](dataObject)
    product.ingredients = dataObject.ingredients
    product.plate = if _.isObject(dataObject.plate) then dataObject.plate else {}
    product.ornamentations = dataObject.ornamentations if product.productType is 'cake'
    product.packageId = dataObject.packageId
    product.packageText = dataObject.packageText
    return product

  _updateStore: ->
    storage.set('basket', "#{JSON.stringify(@products)}")
    storage.set('basketPromoCode', @promoCode)

    if not storage.get 'basketTimestamp'
      storage.set 'basketTimestamp', @makeTimestamp()

    if pageType in ['basket-index', 'basket-order']
      for prod in @products
        @clearEditables(prod.productType)

  _updateHTML: ->
    if not @isEmpty()
      menu = []
      basket = []
      _.each @products, (elem, index) ->
        prod = Basket.getProductInstanceFromDataObject elem

        menu.push Product.renderBasketMenuTemplate prod

        if $('#cart-product').length
          basket.push(Product.renderBasketItemTemplate prod, index)

      $ 'a.basket'
        .text @getAllProductsCount()

      $ '.cart-popup .goods'
        .html menu.join '\n'

      if pageType is 'basket-index'
        $ 'body'
          .removeClass 'empty'

        $ '#cart-product'
          .html """<div class="product-list left">#{basket.join '\n'}</div>#{Product.renderBasketPriceTemplate @getTotalPrice(), @getTotalWeight()}"""

        $ '.cart-elem .package-select'
          .selectmenu width: 128

      if pageType is 'basket-order'
        @updateOrderHTML()
    else
      if @getAllProductsCount() > 0
        $ 'a.basket'
        .text @getAllProductsCount()
      else
        $ 'a.basket'
          .text ""

        $ '.cart-popup'
          .css display: 'none'

      if pageType is 'basket-index'
        if not $('body').hasClass 'empty'
          $('body').addClass 'empty'

        $('#cart-product').html(Basket.renderEmptyBasketTemplate())

  updateOrderHTML: ->
    $ '.total-info'
      .html """#{Product.renderBasketOrderPriceTemplate @getTotalPrice(), @getTotalWeight(), @getTransportPrice()}"""

    $ '.transport-block h3 span'
      .text "#{@getTransportPrice()}"

  isEmpty: ->
    _.isEmpty @products

  _productExists: (product) ->
    result = _.where(
      @products
      productType: product.productType
      baseId: product.baseId
      weight: product.weight
      recommendationTitle: product.recommendationTitle
    )
    res = null

    if res is null
      _.each result, (elem, ind) ->
        if _.isEqual(elem.ingredients, product.ingredients)
          res = result[ind]

          # for chocolate and cake we comparing also plates
          if product.productType in ['chocolate', 'cake'] and _.isEqual(elem.plate, product.plate)
            res = result[ind]

#    if not res
#      res = _.findWhere result, ingredients: product.ingredients
    return res

  clearStorage: ->
    if localStorageSupported
      localStorage.clear()
    else
      deleteAllCookies()

  clearEditables: (productType) ->
    for productType in ['chocolate', 'muesli', 'macarons', 'cake', 'coffee']
      storage.expire "#{productType}Ingredients"
      storage.expire "#{productType}Plate"
      storage.expire "#{productType}Weight"
    storage.expire "uId"

  add: (product) ->
    uId = storage.get 'uId'
    if uId and @products[uId].baseId is product.baseId and @products[uId].productType is product.productType
      @products[uId].ingredients = product.ingredients
      @products[uId].plate = product.plate
      @products[uId].weight = product.weight
      @products[uId].price = product.price
      # for cake
      if typeof product.ornamentations isnt "undefined"
        @products[uId].ornamentations = product.ornamentations
      # delete this title, coz this is NOT recommendation anymore
      @products[uId].recommendationTitle = null
    else
      foundProduct = @_productExists product

      if foundProduct
        foundProduct.quantity += 1
      else
        @products.push product

    @clearEditables()

    @update()

    # and show popup
    Basket.showAddSuccessPopup()

  remove: (product) ->
    foundProduct = @_productExists product

    if foundProduct
      foundProduct.quantity -= 1
      @products = _.without @products, foundProduct if foundProduct.quantity <= 0
    else
      console.warn "No such product: #{product}"

    @update()

  removeAll: (product) ->
    foundProduct = @_productExists product

    if foundProduct
      @products = _.without @products, foundProduct

    @update()

  changePackageText: (product, packageText) ->
    foundProduct = @_productExists product

    if foundProduct
      foundProduct.packageText = packageText
      foundProduct.packageId = 1 unless foundProduct.packageId

    @update()

  changePackage: (product, packageId) ->
    foundProduct = @_productExists product

    if foundProduct
      foundProduct.packageId = parseInt packageId

    @update()

  @showAddSuccessPopup: ->
    $ '.to-cart-popup'
      .css display: 'block'

  @renderEmptyBasketTemplate: ->
    $ '#templates .empty-basket'
      .html()

# this code must be always at bottom
# export to global namespace
global = this
global.Basket = Basket
