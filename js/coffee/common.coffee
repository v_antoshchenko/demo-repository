$ ->
  # init basket
  basket = new Basket()

  ############ COMMON THINGS: SLIDER, TOOLTIPS, ETC ############
  # basket menu
  $ 'a.basket'
    .on 'click', (event) ->
      if not basket.isEmpty()
        $('.cart-popup').toggle()
      else
        window.location.href = $(@).data 'continue' unless pageType is 'basket-index'

  # notifications
  $ '.notification'
    .on 'click', 'div .close', ->
      $nodes = $(@).parents('.notification')
      $nodes.children().first().remove()
      $nodes.children().first().css display: 'table'

  # tooltip
  $ document
    .tooltip
      position:
        my: "center top+2"
        at: "center bottom+7"
        collision: 'none'
        using: (position, feedback) ->
          $ @
            .css position
          $ '<div>'
            .addClass "arr #{feedback.vertical} #{feedback.horizontal}"
            .appendTo @
          return


  $ '.choco-page, .coffee-page, .macarons-page, .muesli-page, .cake-page'
    .tooltip
      position:
        my: "right top+2"
        at: "right bottom+7"
        collision: 'none'
        using: (position, feedback) ->
          $ @
            .css position
          $ '<div>'
            .addClass "arr #{feedback.vertical} #{feedback.horizontal}"
            .appendTo @
          return

  if navigator.userAgent.match(/iPad/i) != null
    $(document).tooltip("option", "disabled", true)

  # slider
  $slider = $('.slider')
  if $slider.length
    $slider.slidesjs height: 190
    $ '.slidesjs-navigation'
    .text ''
    .removeAttr 'title'
    $ '.slidesjs-pagination-item a'
    .text ''

  # tabs
  $tabs = $('.tabs')
  if $tabs.length
    $tabs.tabs()

  # accordion
  $accordion = $('.faq-block')
  if $accordion.length
    $accordion.accordion({
      active: false
      collapsible: true
      heightStyle: "content"
    })
  # change language
  $langForm = $ '#lang-form'
  $ '.lang a'
  .click (event) ->
    event.preventDefault()
    if $(@).attr 'href'
      $langForm.find('input[name=lang]').val $(@).data 'lang'
      $langForm.submit()
  ######################### END ############################

  # product shit selector
  $prods = $ 'div.content-product .clear > div'
  changePrice = (weight) ->
    $prods.each ->
      $(@).find('em').text(
        $(@).data(weight).toString().replace '.', ','
      )
      return
    return

  $ 'div.subscribe-popup i'
    .click ->
      $(@).parents('div.subscribe-popup').css display: 'none'

  $ 'div.thanks-popup i'
    .click ->
      $(@).parents('div.thanks-popup').css display: 'none'

  # check if there is a URL in href and do stuff
  $ '.to-cart-popup .revert'
    .on 'click', ->
      if $(@).attr('href') is "javascript:void(0);"
        $ '.to-cart-popup'
          .css display: 'none'
#
  # we need to memorize input values for input[name=weight] and input[name=grist] (only in coffee)
  changeProdUrl = ->
    [..., weight] = $('ul.radio input[name=weight]:checked').attr('id').split '-'
    if $('ul.radio input[name=grist]').length
      [..., grist] = $('ul.radio input[name=grist]:checked').attr('id').split '-'
    params =
      weight: weight
    params.grist = grist if grist
    $prods.each ->
      $(@).find('a').each ->
        currentHref = $(@).attr 'href'
        currentHref = currentHref.substring(0, currentHref.indexOf('?')) if currentHref.indexOf('?') isnt -1
        $(@).attr('href', "#{currentHref}?#{encodeGETParams params}")

  $ '.weight-panel input[name=weight], .weight-panel input[name=grist]'
    .change ->
      if $(@).attr('name') is 'weight'
        [..., weight] = $(@).attr('id').split '-'
        changePrice(weight)
      changeProdUrl()
      return

  $ '#subscribe'
    .submit (event) ->
      event.preventDefault()
      form = new SubscribeForm '#subscribe'
      form.send()
      $(@).prop 'disabled', true
      return false

#  if pageType is 'choco-index'
#    chocoSavedWeight = Store.get 'chocolateWeight'
#    if chocoSavedWeight
#      $ "#weight-#{chocoSavedWeight}"
#        .prop 'checked', true
#        .change()

  if pageType in ['contact-page', 'corporate-page']
    _getParams = if window.location.search then decodeGETParams(window.location.search) else {}
    if _getParams.thanks
      $ 'div.thanks-popup'
        .css 'display': 'block'
      window.history.pushState({}, 'title', '/')

  if pageType in ['choco-index', 'coffee-index', 'muesli-index', 'cake-index']
    # add recommendation
    $ '.slider .add-recommendation'
    .on 'click', ->
      $prod = $(@).parent().parent()
      product = Basket.getProductInstanceFromDataObject $prod.data()
      basket.add product

    # edit recommendation
    $ '.slider .edit-recommendation'
      .on 'click', ->
        $prod = $(@).parent().parent()

        productType = $prod.data 'productType'
        ingredients = $prod.data 'ingredients'

        Store.set("#{productType}Ingredients", JSON.stringify ingredients)

  # creating product
  if pageType in ['choco-constructor', 'coffee-constructor', 'muesli-constructor', 'macarons-constructor', 'cake-constructor']
    _getParams = if window.location.search then decodeGETParams(window.location.search) else {}

    currentWeight = _getParams.weight
    currentGrist = _getParams.grist

    $productData = $('.product-face').data()

    if pageType is 'choco-constructor'
      $('.product-face span').text if currentWeight is 'kilogram' then "1 #{kg}" else "100 #{g}" # chocolate

    if pageType is 'coffee-constructor'
      $('.product-face span').text if currentWeight is '300' then "300 #{g}" else "150 #{g}" # coffee

    if pageType is 'cake-constructor'
      $('.product-face span').text if currentWeight is '2kg' then "2 #{kg}" else "1 #{kg}" # cake

    $('.price-block .clear p span').text $productData.price

    # check another key
    if pageType is 'choco-constructor'
      product = new Chocolate $productData
      product.updatePlateHTML()

      $ '#choco-own .wrapper div'
        .on 'click', ->
          $(@).parent().find('span.selected').removeClass 'selected'
          $(@).children().addClass 'selected'

          if $('.add-shape-title button').hasClass 'not-complete'
            $('.add-shape-title button').removeClass 'not-complete'

      $ '#choco-own button'
        .on 'click', (event) ->
          event.preventDefault()
          $this = $ @
          $shapeForm = $this.parent()
          if $this.hasClass 'not-complete'
            product.warnUser "#{needToSelectShapeMessage}"
          else if $shapeForm.find('input').val().length < 1
            product.warnUser "#{needToInputTextMessage}"
          else
            if $shapeForm.find('button').data('set') is true
              $shapeForm.removeClass 'unfocus'
              $shapeForm.find('button').attr("data-set", false)
              $shapeForm.find('button').data('set', false)
            else
              shape = $('div#choco-own').find('span.selected').parent().attr 'class'
              text = $(@).parent().find('input').val()
              product.addPlate shape, text
              editText = $shapeForm.find('button').data 'editText'
              $shapeForm.find('button').text(editText).attr("data-set", true)
              $shapeForm.find('button').data('set', true)
              $shapeForm.find('p').text "\"#{text}\""
              $shapeForm.addClass 'unfocus'

    if pageType is 'coffee-constructor'
      product = new Coffee $productData
      product.grist = currentGrist

    if pageType is 'muesli-constructor'
      product = new Muesli $productData

    if pageType is 'macarons-constructor'
      product = new Macarons $productData

    if pageType is 'cake-constructor'
      product = new Cake $productData
      product.addPlate "rect", ""

      $ '.list-product.ornamentations li'
        .on 'click', ->
          $this = $ @

          if $this.is 'li'
            product.addOrnamentation $this.data()
            product.updateProductHTML()

      $ '.decoration-block'
        .on 'click', '.delete', ->
          product.removeAllOrnamentations $(@).parent().data 'id'
          product.updateProductHTML()
          return false

      $ '.list-product.ornamentations li i.minus'
        .on 'click', ->
          product.removeOrnamentation $(@).parents('li').data 'id'
          product.updateProductHTML()
          return false

      $ '#cake-shape-1 .wrap-shapes li'
        .on 'click', ->
          $(@).parent().find('li.added').removeClass 'added'
          $(@).addClass 'added'

          $shapeForm = $('#cake-shape-2 form')

          shape = _.without($(@).attr('class').split(' '), 'added').join ''
          text = $shapeForm.find('input').val()

          product.addPlate shape, text

          # TODO: move thos code inside updatePlateHTML in Cake class
          $shapeBlock = $ '.panel .shape-block'
          .find 'i.shape-figure'
          .removeClass()
          .addClass "shape-figure #{product.plate.shape}"
          $shapeBlock.find('p.title').text("#{product.plate.text}") if product.plate.text

#          if $('.add-shape-title button').hasClass 'not-complete'
#            $('.add-shape-title button').removeClass 'not-complete'

      $ '#cake-shape-2 button'
        .on 'click', (event) ->
          event.preventDefault()
          $this = $ @
          $shapeForm = $this.parent()
          if $this.hasClass 'not-complete'
            product.warnUser "#{needToSelectShapeMessage}"
          else if $shapeForm.find('input').val().length < 1
            product.warnUser "#{needToInputTextMessage}"
          else
            if $shapeForm.find('button').data('set') is true
              $shapeForm.removeClass 'unfocus'
              $shapeForm.find('button').attr("data-set", false)
              $shapeForm.find('button').data('set', false)
            else
              shape = _.without($('div#cake-shape-1').find('li.added').attr('class').split(' '), 'added')[0]
              text = $(@).parent().find('input').val()
              product.addPlate shape, text
              editText = $shapeForm.find('button').data 'editText'
              $shapeForm.find('button').text(editText).attr("data-set", true)
              $shapeForm.find('button').data('set', true)
              $shapeForm.find('p').text "\"#{text}\""
              $shapeForm.addClass 'unfocus'

            $shapeBlock = $ '.panel .shape-block'
            $shapeBlock
              .find 'i.shape-figure'
              .removeClass()
              .addClass "shape-figure #{product.plate.shape}"
            $shapeBlock.find('p').text("#{product.plate.text}") if product.plate.text

    # update HTML
    product.updateProductHTML()

    $ '.constructor'
      .on 'click', '.delete', ->
        product.removeAll $(@).parent().data 'id'
        product.updateProductHTML()
        return false

    $ '.list-product.ingredients li i.minus'
      .on 'click', ->
        product.remove $(@).parents('li').data 'id'
        product.updateProductHTML()
        return false

    # add ingredient to product
    $ '.list-product.ingredients li'
      .on 'click', ->
        $this = $ @

        if $this.is 'li'
          product.add $this.data()
          product.updateProductHTML()

    # storing data in the local storage
    $ '.start-again, .help-type'
      .click ->
          product.saveDataToStorage()

    $ '.to-basket'
      .on 'click', ->
        # add product to basket
        basket.add product._repr()

  if pageType is 'basket-index'
    # basket remove all products
    $ '#cart-product'
      .on 'click', '.cart-elem > a.close', ->
        basket.removeAll Basket.getProductInstanceFromDataObject($(@).parent().data())._repr()

    $ '#cart-product'
      .on 'click', '.count span', ->
        product = Basket.getProductInstanceFromDataObject $(@).parents('.cart-elem').data()
        if $(@).text() is '+'
          basket.add product._repr()
        else
          basket.remove product._repr()

    $ '#cart-product'
      .on 'click', '.package-title > a', ->
        product = Basket.getProductInstanceFromDataObject $(@).parents('.cart-elem').data()
        $(@).after Product.renderPackageInputTemplate(product)
        $(@).parents('.cart-elem').find('.change-package-title input').focus()

    $ '#cart-product'
      .on 'click', '.change-package-title a.close', ->
        $(@).parent().remove()

    $ '#cart-product'
      .on 'click', '.change-package-title button', ->
        product = Basket.getProductInstanceFromDataObject $(@).parents('.cart-elem').data()
        newVal = $(@).parents('.change-package-title').find('input[type=text]').val()
        basket.changePackageText(product._repr(), newVal) if newVal.length > 0

    $ '#cart-product'
      .on 'selectmenuselect', '.package-select', (event, ui) ->
        product = Basket.getProductInstanceFromDataObject $(@).parents('.cart-elem').data()
        basket.changePackage(product._repr(), ui.item.value)

    $ '#cart-product'
      .on 'click', '.cart-elem div.additions.clear a, .cart-elem div.shape-preview a', ->
        product = Basket.getProductInstanceFromDataObject $(@).parents('.cart-elem').data()
        product.saveDataToStorage $(@).parents('.cart-elem').data('uId')

  if pageType is 'basket-order'
    if basket.isEmpty()
      $('.order-form form').find('input, textarea, select, a, button, span.ui-selectmenu-button').addClass 'unactive'
      messageUser(basketIsEmptyMessage, 'red', false)
    else
      $ 'select[name=city]'
      .on 'selectmenuchange', ->
        basket.updateOrderHTML()

      $ '.order-form'
        .submit (event) ->
          event.preventDefault()
          form = new OrderForm '.order-form form'
          form.send()
          $(@).prop 'disabled', true
          return false
      return false

