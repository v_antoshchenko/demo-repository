class BaseForm
  constructor: (selector) ->
    @selector = selector
    @method = 'post'
    @url = window.location.href
    @requiredFields = null

    throw "ValueError: selector parameter must be string, got \"#{typeof @selector}\"" unless typeof @selector is "string"

    formJQObject = $(@selector)
    throw "DoesNotExist: form with selector \"#{@selector}\"" unless not _.isEmpty(formJQObject)
    throw "MultipleObjectsReturned: there are multiple forms \"#{@selector}\"" if formJQObject.length > 1

    @form = formJQObject.serializeArray()

  save: ->
    throw "NotImplementedError: implement this method"

class OrderForm extends BaseForm
  constructor: (selector) ->
    super selector
    @url = saveOrderURL
    @requiredFields = ['first_name', 'last_name', 'phone', 'email', 'date', 'time', 'city', 'address']

  send: ->
    basket = new Basket()
    $.ajax
      type: "post"
      dataType: 'json'
      headers: "X-CSRFToken": getCookie "csrftoken"
      url: @url
      data:
        form_data: JSON.stringify(@form),
        order_data: JSON.stringify(basket.products)
      statusCode:
        400: (data) =>
          $('input, textarea').parent().removeClass 'error'
          _.each data.responseJSON.errors, (value, key, list) ->
            $ "input[name=#{key}], textarea[name=#{key}]"
              .parent().addClass 'error'
          $("#{@selector} button[type=submit]").prop 'disabled', false
        200: (response) ->
          basket.clearStorage()
          window.location.replace response.redirect_to
        500: (response) ->
          alert('Sorry, there is an error')

class SubscribeForm extends BaseForm
  constructor: (selector) ->
    super selector
    @url = subscribeURL
    @requiredFields = ['email']

  send: ->
    $.ajax
      type: "post"
      dataType: 'json'
      headers:
        "X-CSRFToken": getCookie "csrftoken"
      url: @url
      data: @form
      statusCode:
        400: (data) =>
          $('input, textarea').parent().removeClass 'error'
          _.each data.responseJSON.errors, (value, key, list) ->
            $ "input[name=#{key}]"
            .parent().addClass 'error'
          $("#{@selector} button[type=submit]").prop 'disabled', false
        200: (response) =>
          $('div.subscribe-popup').css display: 'block'
          $("#{@selector}")[0].reset()
        500: (response) ->
          alert('Sorry, there is an error')


# this code must be always at bottom
# export to global namespace
global = this
global.OrderForm = OrderForm
global.SubscribeForm = SubscribeForm
