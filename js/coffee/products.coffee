arraySum = (arr) ->
  _.reduce(
    arr
    (prev, curr) ->
      prev + curr
    0
  )

class Product
  constructor: (data) ->
    # default parameters
    @maxInOnePosition = @maxInOnePosition or 3
    @maxIngredients = @maxIngredients or 5
    @productType = @productType or null
    @packageId = 1
    @packageText = null
    @ingredients = []

    # data parameters
    @baseId = data.baseId
    @title = data.title
    @recommendationTitle = data.recommendationTitle or null
    @image = data.image
    @price = parseFloat data.price
    @weight = data.weight or 0
    @energy = data.energy or 0
    @fats = data.fats or 0
    @carbohydrates = data.carbohydrates or 0
    @quantity = data.quantity or 1

    throw "ValueError: type of product cannot be null" unless @productType
    throw "ValueError: maxIngredients cannot be null" unless @maxIngredients

  _ingredientExists: (ingredientId) ->
    _.findWhere @ingredients, id: ingredientId

  _getIngredients: ->
    ingrs = Store.get "#{@productType}Ingredients"
    if ingrs
      @ingredients = JSON.parse ingrs

  _repr: ->
    baseId: @baseId
    title: @title
    recommendationTitle: @recommendationTitle
    image: @image
    price: @price
    weight: @weight
    ingredients: @ingredients
    productType: @productType
    quantity: @quantity
    packageId: @packageId
    packageText: @packageText

  displayTitle: ->
    if @recommendationTitle
      return @recommendationTitle
    return @title

  getPrice: ->
    # base price
    price = parseFloat @price
    # ingredients prices
    ingredientPrices = _.map @ingredients, (elem) -> (elem.price / 100) * elem.quantity
    if ingredientPrices.length > 0
      price += arraySum ingredientPrices
    return price

  # coz it's grams, motherfucker
  getWeight: ->
    # base weight
    weight = parseInt @weight
    # ingredients weight
    ingredientWeight = _.map @ingredients, (elem) -> elem.weight * elem.quantity
    if ingredientWeight.length > 0
      weight += arraySum ingredientWeight
    return weight / 1000

  getEnergy: ->
    # base energy value
    energy = @energy
    # ingredients energy
    ingredientEnergy = _.map @ingredients, (elem) -> elem.energy * elem.quantity
    if ingredientEnergy.length > 0
      energy += arraySum ingredientEnergy
    return energy

  getCarbohydrates: ->
    # base carbohydrates
    carbohydrates = @carbohydrates
    # ingredients carbohydrates
    ingredientCarbohydrates = _.map @ingredients, (elem) ->
      elem.carbohydrates * elem.quantity
    if ingredientCarbohydrates.length > 0
      carbohydrates += arraySum ingredientCarbohydrates
    return carbohydrates

  getFats: ->
    # base fats
    fats = @fats
    # ingredients fats
    ingredientFats = _.map @ingredients, (elem) ->
      elem.fats * elem.quantity
    if ingredientFats.length > 0
      fats += arraySum ingredientFats
    return fats

  getFullPrice: ->
    return @getPrice() * @quantity

  getFullWeight: ->
    return @getWeight() * @quantity

  getMeasurementNum: ->
    @weight

  getMeasurementUnit: ->
    throw "NotImplementedError: cannot figure out which var to use for unit"

  @renderBasketMenuTemplate: (product) ->
    """
    <div class="#{product.productType}">
      <p class="title">#{product.quantity} x #{product.displayTitle()}</p>
      <div class="bottom clear">
        <span>#{product.getMeasurementNum()}#{product.getMeasurementUnit()}</span>
        <span>#{product.getFullPrice().toFixed(2).replace '.', ','} €</span>
      </div>
    </div>
    """

  @renderProductIngredientTemplate: (id, quantity, imgUrl, title = null) ->
    if title isnt null
      bg = "background-color: #{imgUrl}"
      titleP = """<p class="title">#{title}</p>"""
      quantityHTML = ""
    else
      bg = "background-image: url(#{imgUrl})"
      titleP = ""
      quantityHTML = """<p class="quantity">x
        <i>#{quantity}</i>
      </p>"""
    """
    <div style="#{bg}" data-id="#{id}">
      #{quantityHTML}
      <span class="delete"></span>#{titleP}
    </div>
    """

  @renderBasketItemTemplate: (product, uId = null) ->
    renderedPackage = Product.renderPackageTemplate product
    packageId = $(renderedPackage).find('option').first().val()
    """
    <div class="cart-elem #{product.productType}" data-product-type="#{product.productType}" data-title="#{product.title}" data-recommendation-title="#{product.recommendationTitle}" data-weight="#{product.weight}" data-base-id="#{product.baseId}" data-image="#{product.image}" data-price="#{product.price}" data-ingredients="#{_.escape JSON.stringify(product.ingredients)}" data-u-id="#{uId}" data-plate="#{_.escape JSON.stringify(product.plate)}" data-package-id="#{if product.packageId then product.packageId else packageId}" data-package-text="#{if product.packageText then product.packageText else ''}" data-energy="#{product.energy}" data-fats="#{product.fats}" data-carbohydrates="#{product.carbohydrates}">
      <div class="main">
        <div class="img"><img src="#{product.image}" alt="#{product.title}"></div>
        <div class="basket-content">
          <p class="product-title">#{product.displayTitle()} <span>#{product.getMeasurementNum()}#{product.getMeasurementUnit()}</span></p>
          <div class="additions clear">
            <p class="title">#{lisandid}:</p>
            #{Product.renderBasketItemIngredients product.ingredients}
            <a href="#{Product.renderReturnToConstructorLink product}" class="left">#{muuda}</a>
          </div>
          #{Product.renderBasketPlateTemplate product}
          #{renderedPackage}
          #{Product.renderPackageTextTemplate product}
        </div>
        <div class="basket-price">
          <div class="count clear">
            <span>-</span>
            <p>#{product.quantity}</p>
            <span>+</span>
          </div>
          <div class="price">
            <span>#{product.getFullPrice().toFixed(2).replace '.', ','}</span>€
          </div>
        </div>
      </div>
      <a class="close" href="javascript:void(0);"></a>
    </div>
    """

  @renderReturnToConstructorLink: (product) ->
    params = {}
    if product.productType is 'macarons'
      return $("#templates .wrapper a.#{product.productType}").attr('href') + "/r"
    if product.productType is 'chocolate'
      params.weight = if product.weight is 1 then 'kilogram' else 'gram'
    if product.productType is 'coffee'
      params.weight = product.weight
      params.grist = product.grist
    if product.productType is 'cake'
      params.weight = if product.weight is 1 then '1kg' else '2kg'
    paramsString = if not _.isEmpty params then "?#{encodeGETParams params}" else ""
    return $("#templates .wrapper a.#{product.productType}").attr('href') + "/r/#{product.baseId}#{paramsString}"

  @renderBasketPriceTemplate: (totalPrice, totalWeight) ->
    $template = $ "<div>#{$('#templates .basket-total').html()}</div>"

    $template.find("p.price span").text convertToFixed(totalPrice)
    $template.find("p.weight span").text convertToFixed(totalWeight)

    $template.html()

  @renderBasketOrderPriceTemplate: (totalPrice, totalWeight, transportPrice) ->
    $template = $ "<div>#{$('#templates .total').html()}</div>"

    $template.find(".first .tooted span").text convertToFixed(totalPrice)
    $template.find(".first .transport span").text convertToFixed(transportPrice)

    $template.find(".left .in-total span").text convertToFixed(totalPrice + transportPrice)
    $template.find(".left .calories span").text convertToFixed(totalWeight)

    $template.html()

  @renderBasketPlateTemplate: (product) ->
    if product.productType not in ['chocolate', 'cake'] or _.isEmpty(product.plate)
      return ''

    params = {}
    paramsString = if not _.isEmpty params then "?#{encodeGETParams params}" else ""
    link = $("#templates .wrapper a.#{product.productType}").attr('href') + "/r/#{product.baseId}#{paramsString}"
    if product.productType is 'choco'
      link += '#choco-own'
    else if product.productType is 'cake'
      link += '#cake-shape-2'

    $template = $ "<div>#{$('#templates .shape').html()}</div>"
    $template.find("div.shape-preview").addClass "#{product.plate.shape}"
    $template.find("div.shape-preview i").attr('title', product.plate.text)
    $template.find("div.shape-preview a").attr('href', link)
    $template.html()

  @renderBasketItemIngredients: (ingredients) ->
    if not ingredients
      return ''
    compiled = _.template "<% _.each(ingredients, function(elem) { %> <li><%= elem.title %></li> <% }); %>"
    "<ul>#{compiled 'ingredients': ingredients}</ul>"

  @renderPackageTemplate: (product) ->
    $template = $ "<div>#{$('#' + product.productType + '-package').html()}</div>"

    if product.packageId
      $template.find("option[value=#{product.packageId}]").attr('selected', 'selected')
    else
      $template.find('option').first().attr('selected', 'selected')

    return "#{$template.html()}"

  @renderPackageTextTemplate: (product) ->
    """
    <div class="package-title">
      <a href="javascript:void(0);">#{if product.packageText then product.packageText else "+ #{lisaPackendile}"}</a>
    </div>
    """

  @renderPackageInputTemplate: (product) ->
    $template = $ "<div>#{$('#templates .change-package-text').html()}<div>"
    $template.find('input').attr "value", "#{if product.packageText then product.packageText else ''}"
    $template.html()

  updateProductHTML: ->
    ingrs = []
    $('.list-product li').removeClass 'added'

    _.each @ingredients, (elem) =>
      if @productType is 'macarons'
        ingrs.push Product.renderProductIngredientTemplate elem.id, elem.quantity, elem.img, elem.title
      else
        ingrs.push Product.renderProductIngredientTemplate elem.id, elem.quantity, elem.img

      $(".list-product.ingredients li[data-id=#{elem.id}]").addClass 'added'

    emptyPositionsNum = @maxIngredients - ingrs.length

    _.times emptyPositionsNum, ->
      ingrs.push('<div></div>')

    $ '.constructor'
    .html ingrs.join '\n'

    # update price info
    $ '.price-block'
    .find '.clear > p > span'
    .text @getPrice().toFixed(2).replace '.', ','

    # update energy value
    $ '.price-block .energy'
    .find 'p'
    .first().find 'span'
    .text @getEnergy()

    # update carbohydrates value
    $ '.price-block .energy'
    .find 'p'
    .eq 1
    .find 'span'
    .text @getCarbohydrates()

    # update fats value
    $ '.price-block .energy'
    .find 'p'
    .last().find 'span'
    .text @getFats()

  saveDataToStorage: (uId = null) ->
    Store.set("#{@productType}Ingredients", JSON.stringify @ingredients)

    if uId isnt null
      Store.set 'uId', parseInt uId

  warnDeveloper: (message) ->
    console.warn message

  warnUser: (message) ->
    messageUser(message, 'red')

  add: (data) ->
    foundIngredient = @_ingredientExists data.id

    if foundIngredient
      if foundIngredient.quantity < @maxInOnePosition
        foundIngredient.quantity += 1
      else
        @warnDeveloper 'Maximum ingredients per position reached'
        @warnUser "#{maxIngredientsMessage}"
    else
      if @ingredients.length < @maxIngredients
        @ingredients.push(
          id: data.id
          title: data.title
          price: data.price
          weight: data.weight
          img: data.imgurl
          energy: data.energy
          carbohydrates: data.carbohydrates
          fats: data.fats
          quantity: 1
        )
      else
        @warnDeveloper "Maximum ingredients in one product reached"
        @warnUser "#{maxIngredientsInProduct} #{@maxIngredients}"

  remove: (ingredientId) ->
    foundIngredient = @_ingredientExists ingredientId

    if foundIngredient
      foundIngredient.quantity -= 1
      @ingredients = _.without @ingredients, foundIngredient if foundIngredient.quantity <= 0
    else
      @warnDeveloper "No ingredient with id: #{ingredientId}"

  removeAll: (ingredientId) ->
    foundIngredient = @_ingredientExists ingredientId

    if foundIngredient
      @ingredients = _.without @ingredients, foundIngredient

class Chocolate extends Product
  constructor: (data) ->
    @productType = 'chocolate'

    super data

    @_getIngredients()

    if Store.get "#{@productType}Plate"
      @plate = JSON.parse(Store.get "#{@productType}Plate")
    else
      @plate = {}

  getMeasurementUnit: ->
    if @weight is 100 then "#{g}" else "#{kg}"

  _repr: ->
    tmp_repr = super()
    tmp_repr.plate = @plate
    return tmp_repr

  updatePlateHTML: ->
    if not _.isEmpty @plate
      $ "#choco-own div.#{@plate.shape}"
      .find 'span'
      .addClass 'selected'

      $shapeForm = $ "#choco-own form.add-shape-title"
      editText = $shapeForm.find('button').data 'editText'
      $shapeForm.addClass 'unfocus'
      $shapeForm.find('button').text(editText).attr("data-set", true)
      $shapeForm.find('p').text "\"#{@plate.text}\""
      $shapeForm.find('input').val "#{@plate.text}"
      $shapeForm.find('button').removeClass 'not-complete'

  addPlate: (shape, text) ->
    @plate.shape = shape
    @plate.text = text

  saveDataToStorage: (uId = null) ->
    currWeight = if @weight is 1 then 'kilogram' else 'gram'
    Store.set("#{@productType}Weight", currWeight)
    Store.set "#{@productType}Plate", JSON.stringify @plate
    super uId

  getWeight: ->
    # check if it's big chocolate
    if @weight is 1
      weight = 1
      # ingredients weight
      ingredientWeight = _.map @ingredients, (elem) ->
        (elem.weight * 0.001) * elem.quantity
      if ingredientWeight.length > 0
        weight += _.reduce(
          ingredientWeight
          (prev, curr) ->
            prev + curr
          0
        )
      return weight
    else
      super()

class Coffee extends Product
  constructor: (data) ->
    @productType = 'coffee'
    @maxIngredients = 4
    @grist = 'ground'

    super data

    @_getIngredients()

  saveDataToStorage: (uId = null) ->
    currWeight = if @weight is 1 then 300 else 150
    Store.set("#{@productType}Weight", currWeight)
    super uId

  getMeasurementUnit: ->
    "#{g}"

  _repr: ->
    baseId: @baseId
    title: @title
    recommendationTitle: @recommendationTitle
    image: @image
    price: @price
    weight: @weight
    ingredients: @ingredients
    productType: @productType
    quantity: @quantity
    packageId: @packageId
    packageText: @packageText
    grist: @grist

class Muesli extends Product
  constructor: (data) ->
    @productType = 'muesli'
    @maxIngredients = 5

    super data

    @_getIngredients()

  getMeasurementUnit: ->
    "#{g}"

class Macarons extends Product
  constructor: (data) ->
    @productType = 'macarons'
    @maxIngredients = 7
    @maxInOnePosition = 7


    super data

    @_getIngredients()

  getMeasurementUnit: ->
    "#{pcs}"

  getMeasurementNum: ->
    arraySum _.map @ingredients, (elem) ->
      elem.quantity

  add: (data) ->
    if @ingredients.length < @maxIngredients
      @ingredients.push(
        id: data.id
        title: data.title
        price: data.price
        weight: data.weight
        img: data.imgurl
        energy: data.energy
        carbohydrates: data.carbohydrates
        fats: data.fats
        quantity: 1
      )
    else
      @warnDeveloper "Maximum ingredients in one product reached"
      @warnUser "#{maxIngredientsInProduct} #{@maxIngredients}"


class Cake extends Product
  constructor: (data) ->
    @productType = 'cake'
    @maxIngredients = 4
    @maxOrnamentations = 3
    @ornamentations = []

    super data

    @_getIngredients()

    if Store.get "#{@productType}Plate"
      @plate = JSON.parse(Store.get "#{@productType}Plate")
    else
      @plate = {}

  _ornamentationExist: (ornamentId) ->
    _.findWhere @ornamentations, id: ornamentId

  _getIngredients: ->
    ornmns = Store.get "#{@productType}Ornamentations"
    if ornmns
      @ornamentations = JSON.parse ornmns
    super()

  getMeasurementUnit: ->
    "#{kg}"

  getWeight: ->
    weight = @weight
    ingredientWeight = _.map @ingredients, (elem) ->
      (elem.weight * 0.001) * elem.quantity
    if ingredientWeight.length > 0
      weight += _.reduce(
        ingredientWeight
        (prev, curr) ->
          prev + curr
        0
      )
    return weight

  addOrnamentation: (data) ->
    foundIngredient = @_ornamentationExist data.id

    if foundIngredient
      if foundIngredient.quantity < @maxInOnePosition
        foundIngredient.quantity += 1
      else
        @warnDeveloper 'Maximum ingredients per position reached'
        @warnUser "#{maxIngredientsMessage}"
    else
      if @ornamentations.length < @maxOrnamentations
        @ornamentations.push(
          id: data.id
          title: data.title
          price: data.price
          weight: data.weight
          img: data.imgurl
          energy: data.energy
          carbohydrates: data.carbohydrates
          fats: data.fats
          quantity: 1
        )
      else
        @warnDeveloper "Maximum ingredients in one product reached"
        @warnUser "#{maxIngredientsInProduct} #{@maxIngredients}"

  removeOrnamentation: (ornamentId) ->
    foundIngredient = @_ornamentationExist ornamentId

    if foundIngredient
      foundIngredient.quantity -= 1
      @ornamentations = _.without @ornamentations, foundIngredient if foundIngredient.quantity <= 0
    else
      @warnDeveloper "No ingredient with id: #{ingredientId}"

  removeAllOrnamentations: (ornamentId) ->
    foundIngredient = @_ornamentationExist ornamentId

    if foundIngredient
      @ornamentations = _.without @ornamentations, foundIngredient

  updateProductHTML: ->
    super()
    ornmnts = []
    _.each @ornamentations, (elem) =>
      ornmnts.push @renderOrnamentationTemplate elem
      $(".list-product.ornamentations li[data-id=#{elem.id}]").addClass 'added'

    emptyOrnamentationPositionsNum = @maxOrnamentations - ornmnts.length

    _.times emptyOrnamentationPositionsNum, ->
      ornmnts.push('<div></div>')

    $ '.decoration-block'
      .html ornmnts.join '\n'

  renderOrnamentationTemplate: (elem) ->
    """
    <div data-id="#{elem.id}">
      <p class="quantity">x
        <i>#{elem.quantity}</i>
      </p>
      <span class="delete"></span>
      <img src="#{elem.img}" />
    </div>
    """

  _repr: ->
    tmp_repr = super()
    tmp_repr.plate = @plate
    return tmp_repr

  updatePlateHTML: ->
    if not _.isEmpty @plate
      $ "#choco-own div.#{@plate.shape}"
      .find 'span'
      .addClass 'selected'

      $shapeForm = $ "#choco-own form.add-shape-title"
      editText = $shapeForm.find('button').data 'editText'
      $shapeForm.addClass 'unfocus'
      $shapeForm.find('button').text(editText).attr("data-set", true)
      $shapeForm.find('p').text "\"#{@plate.text}\""
      $shapeForm.find('input').val "#{@plate.text}"
      $shapeForm.find('button').removeClass 'not-complete'

  addPlate: (shape, text) ->
    @plate.shape = shape
    @plate.text = text

  saveDataToStorage: (uId = null) ->
    currWeight = if @weight is 1 then 'kilogram' else 'gram'
    Store.set("#{@productType}Weight", currWeight)
    Store.set "#{@productType}Plate", JSON.stringify @plate
    super uId

  _repr: ->
    tmp_repr = super()
    tmp_repr.plate = @plate
    tmp_repr.ornamentations = @ornamentations
    return tmp_repr

  getPrice: ->
    price = super()
    ornamentsPrices = _.map @ornamentations, (elem) ->
      (elem.price / 100) * elem.quantity
    if ornamentsPrices.length > 0
      price += arraySum ornamentsPrices
    return price

  # coz it's grams, motherfucker
  getWeight: ->
    weight = @weight
    # ingredients weight
    ingredientWeight = _.map @ingredients, (elem) ->
      (elem.weight * 0.001) * elem.quantity
    if ingredientWeight.length > 0
      weight += _.reduce(
        ingredientWeight
        (prev, curr) ->
          prev + curr
        0
      )
    ornamentsWeight = _.map @ornamentations, (elem) ->
      (elem.weight * 0.001) * elem.quantity
    if ornamentsWeight.length > 0
      weight += _.reduce(
        ornamentsWeight
        (prev, curr) ->
          prev + curr
        0
      )
    return weight

  getEnergy: ->
    energy = super()
    ornamentsEnergy = _.map @ornamentations, (elem) ->
      elem.energy * elem.quantity
    if ornamentsEnergy.length > 0
      energy += arraySum ornamentsEnergy
    return energy

  getCarbohydrates: ->
    carbohydrates = super()
    ornamentsCarbohydrates = _.map @ornamentations, (elem) ->
      elem.carbohydrates * elem.quantity
    if ornamentsCarbohydrates.length > 0
      carbohydrates += arraySum ornamentsCarbohydrates
    return carbohydrates

  getFats: ->
    fats = super()
    ornamentsFats = _.map @ornamentations, (elem) ->
      elem.fats * elem.quantity
    if ornamentsFats.length > 0
      fats += arraySum ornamentsFats
    return fats


# this code must be always at bottom
# export to global namespace
global = this
global.Product = Product
global.Chocolate = Chocolate
global.Coffee = Coffee
global.Muesli = Muesli
global.Macarons = Macarons
global.Cake = Cake
