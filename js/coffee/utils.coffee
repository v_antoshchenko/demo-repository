getCookie = (name) ->
  cookies = document.cookie.split /;\s*/
  for cookie in cookies
    currentParts = cookie.split "="
    currentName = decodeURIComponent(currentParts[0])
    if currentName == name
      return decodeURIComponent(currentParts[1])
  return null

convertToFixed = (float) ->
  float.toFixed(2).replace '.', ','

`function deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}`

localStorageSupported = do ->
  try
    `(('localStorage' in window) && window['localStorage'] !== null)`
  catch e
    false

# gets str like '?aa=bb&cc=dd' and returns object
decodeGETParams = (strParams) ->
  if strParams is ""
    return {}
  JSON.parse("{\"#{
  decodeURI strParams
  .replace /\?/g, ''
  .replace /" /g, '\\"'
  .replace /&/g, '","'
  .replace /\=/g, '":"'
  }\"}")

# gets obj like {aa: 'bb', cc: 'dd'} and returns str
encodeGETParams = (objParams) ->
  $.param(objParams)

messageUser = (message, status='yellow', can_close=true) ->
  html = if can_close then "<div class=\"red\"><i class=\"close\"></i><p>#{message}</p></div>" else "<div class=\"red\"><p>#{message}</p></div>"
  if $('.notification').children().length
    $ '.notification'
    .children().css display: 'none'
    $(html).prependTo $('.notification')
  else
    $('.notification').append html

# a simple wrapper around html5 local storage and cookies
# provides three methods to interact with data
Store = do ->
  if localStorageSupported
    # will displace data until it can successfully save
    safeSet = (key, value) ->
      try
        localStorage.setItem key, value
        value
      catch e
        for num in [0..5]
          localStorage.removeItem localStorage.key localStorage.length - 1
        safeSet key, value
    {
    set: safeSet

    get: (key) ->
      localStorage[key]

    expire: (key) ->
      value = localStorage[key]
      localStorage.removeItem(key)
      value
    }
  else
    createCookie = (name, value, days) ->
      if days
        date = new Date
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
        expires = "; expires=" + date.toGMTString()
      else
        expires = ""

      document.cookie = name + "=" + value + expires + "; path=/"

      value

    getCookie = (key) ->
      key = key + "="
      for cookieFragment in document.cookie.split(';')
        return cookieFragment.replace(/^\s+/, '').substring(key.length + 1,
          cookieFragment.length) if cookieFragment.indexOf(key) == 0
      return null

    {
    set: (key, value) ->
      createCookie key, value, 1

    get: getCookie

    expire: (key) ->
      value = Store.get key
      createCookie key, "", -1
      value
    }

# this code must be always at bottom
# export to global namespace
global = this
global.Store = Store
global.localStorageSupported = localStorageSupported
global.deleteAllCookies = deleteAllCookies
global.decodeGETParams = decodeGETParams
global.encodeGETParams = encodeGETParams
global.messageUser = messageUser
global.getCookie = getCookie
global.convertToFixed = convertToFixed
