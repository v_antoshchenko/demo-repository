from django.contrib import admin
from adminsortable.admin import SortableAdminMixin
from hvad.admin import TranslatableAdmin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline
from django.utils.translation import ugettext_lazy as _

from mecusto.admin import mecusto_admin
import models


@admin.register(models.Package, site=mecusto_admin)
class PackageAdmin(SortableAdminMixin, TranslatableAdmin):
    list_display = '__unicode__', 'product'

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.City, site=mecusto_admin)
class CityAdmin(SortableAdminMixin, TranslatableAdmin):
    def has_delete_permission(self, request, obj=None):
        return False


class ChocolateIngredientInline(NestedTabularInline):
    model = models.ChocolateIngredient
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class ChocolateInline(NestedStackedInline):
    model = models.Chocolate
    extra = 0
    max_num = 0
    inlines = ChocolateIngredientInline,
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return (
            field for field in self.model._meta.get_all_field_names() if field not in (
            'order', 'order_id', 'base_id', 'chocolateingredients', 'package_id', 'shape_label', 'label'
        ))


class CoffeeIngredientInline(NestedTabularInline):
    model = models.CoffeeIngredient
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class CoffeeInline(NestedStackedInline):
    model = models.Coffee
    extra = 0
    max_num = 0
    inlines = CoffeeIngredientInline,
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return (
            field for field in self.model._meta.get_all_field_names() if field not in (
            'order', 'order_id', 'base_id', 'coffeeingredients', 'package_id'
        ))


class MuesliIngredientInline(NestedTabularInline):
    model = models.MuesliIngredient
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class MuesliInline(NestedStackedInline):
    model = models.Muesli
    extra = 0
    max_num = 0
    inlines = MuesliIngredientInline,
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return (
            field for field in self.model._meta.get_all_field_names() if field not in (
            'order', 'order_id', 'base_id', 'muesliingredients', 'package_id'
        ))


class MacaronsIngredientInline(NestedTabularInline):
    model = models.MacaronsIngredient
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class MacaronsInline(NestedStackedInline):
    model = models.Macarons
    extra = 0
    max_num = 0
    inlines = MacaronsIngredientInline,
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return (
            field for field in self.model._meta.get_all_field_names() if field not in (
            'order', 'order_id', 'base_id', 'macaronsngredients', 'package_id'
        ))


class CakeIngredientInline(NestedTabularInline):
    model = models.CakeIngredient
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class CakeOrnamentationInline(NestedTabularInline):
    model = models.CakeOrnamentation
    extra = 0
    max_num = 0
    can_delete = False
    fields = 'ingredient', 'quantity'

    def get_readonly_fields(self, request, obj=None):
        return (field for field in self.model._meta.get_all_field_names() if
                field not in ('item', 'item_id', 'ingredient_id'))


class CakeInline(NestedStackedInline):
    model = models.Cake
    extra = 0
    max_num = 0
    inlines = CakeIngredientInline, CakeOrnamentationInline
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return (
            field for field in self.model._meta.get_all_field_names() if field not in (
            'order', 'order_id', 'base_id', 'package_id', 'shape_label', 'label', 'cakeingredient', 'cakeornamentation'
        ))


@admin.register(models.Order, site=mecusto_admin)
class OrderAdmin(NestedModelAdmin):
    list_display = 'id', 'cost', 'order_date', 'email'
    fieldsets = (
        (
            _('Order Information'), {
                'fields': (
                    ('cost', 'delivery_cost', 'promo_code'),
                    'payment_method', 'order_date', 'status', 'manager_comment', 'mail_delivered'
                )
            }
        ),
        (
            _('Customer Information'), {
                'fields': (
                    ('first_name', 'last_name'),
                    ('email', 'phone'),
                    'customer_language'
                )
            }
        ),
        (
            _('Delivery Information'), {
                'fields': (
                    ('delivery_date', 'delivery_time'),
                    'city', 'address', 'customer_comment'
                )
            }
        )
    )
    inlines = ChocolateInline, CoffeeInline, MuesliInline, MacaronsInline, CakeInline

    # TODO make this not so ugly
    def get_readonly_fields(self, request, obj=None):
        return 'first_name', 'last_name', 'email', 'phone', 'city', 'address', 'customer_comment', \
               'promo_code', 'original_cost', 'cost', 'order_date', 'payment_method', \
               'delivery_date', 'delivery_time', 'mail_delivered', 'customer_language'

    def has_add_permission(self, request):
        if request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False
