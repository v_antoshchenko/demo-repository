from django.conf.urls import patterns, url

urlpatterns = patterns('basket.api.views',
   url(r'^/save-order$', 'save_order_api', name='save-order'),
)
