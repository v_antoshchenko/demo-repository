from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.db.transaction import atomic
from django.http.response import JsonResponse
from django.utils.encoding import iri_to_uri
from django.views.generic import FormView
import datetime
import itertools
from django.utils import timezone
from django.utils import translation
import json

from basket import models, settings
from common.models import Subscriber
from ..forms import OrderValidationForm
from ..helpers import (AvailabilityHelper, BaseHelper, ChocolateHelper, CoffeeHelper, MuesliHelper,
                       MacaronsHelper, CakesHelper)


class SaveOrderAPI(FormView):
    form_class = OrderValidationForm
    success_url = reverse_lazy('basket:success-page')
    order_model = models.Order
    chocolate_models = {
        'base': models.Chocolate,
        'ingredients': models.ChocolateIngredient
    }
    coffee_models = {
        'base': models.Coffee,
        'ingredients': models.CoffeeIngredient
    }
    muesli_models = {
        'base': models.Muesli,
        'ingredients': models.MuesliIngredient
    }
    macarons_models = {
        'base': models.Macarons,
        'ingredients': models.MacaronsIngredient
    }
    cake_models = {
        'base': models.Cake,
        'ingredients': models.CakeIngredient,
        'ornamentations': models.CakeOrnamentation
    }

    def __init__(self, *args, **kwargs):
        super(SaveOrderAPI, self).__init__(**kwargs)

        self.chocolate_products = []
        self.coffee_products = []
        self.muesli_products = []
        self.macarons_products = []
        self.cake_products = []
        self.products = []

    def get(self, request, *args, **kwargs):
        raise PermissionDenied()

    def get_form_kwargs(self):
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
        }

        if self.request.method in ('POST', 'PUT'):
            form_data = json.loads(self.request.POST.get('form_data'))
            data = dict((item['name'], item['value']) for item in form_data)

            kwargs.update({
                'data': data,
                # no files here
                # 'files': self.request.FILES,
            })
        return kwargs

    def form_invalid(self, form):
        return JsonResponse({'errors': form.errors}, status=400)

    def form_valid(self, form):
        self.save_order(form, order_data=json.loads(self.request.POST.get('order_data', [])))
        return JsonResponse({'status': 'OK', 'redirect_to': iri_to_uri(self.get_success_url())})

    def collect_products(self, order_data):
        BaseHelper.proxy_helper = AvailabilityHelper(order_data)
        for item in order_data:
            if item['productType'] == 'chocolate':
                self.chocolate_products.append(ChocolateHelper(item))
            elif item['productType'] == 'coffee':
                self.coffee_products.append(CoffeeHelper(item))
            elif item['productType'] == 'muesli':
                self.muesli_products.append(MuesliHelper(item))
            elif item['productType'] == 'macarons':
                self.macarons_products.append(MacaronsHelper(item))
            elif item['productType'] == 'cake':
                self.cake_products.append(CakesHelper(item))

        # so we could iterate it twice or more times
        self.products = list(itertools.chain(
            self.chocolate_products,
            self.coffee_products,
            self.muesli_products,
            self.macarons_products,
            self.cake_products
        ))

    def get_order_cost(self, order_data):
        total_cost = 0.0

        for product in self.products:
            total_cost += product.full_price

        return total_cost

    @staticmethod
    def calculate_commission(order_cost, commission=20.0):
        """
        Charges commission of 20% from order_cost
        :param order_cost: float
        :param commission: 20% by default
        :return: float commission to be charged
        """
        return (commission * order_cost) / 100.0

    # FIXME: refactor this code to not save items separately
    @atomic
    def save_order(self, form, order_data=None):
        # prepare to rock!1111
        self.collect_products(order_data)

        # get all data
        lang = translation.get_language_from_request(self.request, check_path=True)
        data = form.cleaned_data
        order_cost = self.get_order_cost(order_data)
        city = models.City.objects.get(id=data['city'])
        now = timezone.now()
        # delivery_cost = self.calculate_commission(order_cost)  # switched off (for now)
        delivery_cost = 0

        # update defaults with new values
        # with following conditions
        if city.delivery_price:
            delivery_cost += city.delivery_price

        order = self.order_model.objects.create(
            original_cost=order_cost + delivery_cost,
            cost=order_cost + delivery_cost,  # same as above (for now)
            delivery_cost=delivery_cost,
            first_name=data['first_name'],
            last_name=data['last_name'],
            phone=data['phone'],
            email=data['email'],
            order_date=now,
            delivery_date=datetime.datetime.strptime(data['date'], '%d.%m.%Y'),
            delivery_time=data['time'],
            city_id=data['city'],
            address=data['address'],
            customer_comment=data['comment'],
            promo_code=data['promo_code'],
            status=settings.ORDER_STATUS_ACCEPTED,
            payment_method=data['payment_method'],
            customer_language=lang,
        )

        self.save_chocolate(order.id, chocolates=self.chocolate_products or None)
        self.save_coffee(order.id, coffee=self.coffee_products or None)
        self.save_muesli(order.id, muesli=self.muesli_products or None)
        self.save_macarons(order.id, macarons=self.macarons_products or None)
        self.save_cake(order.id, cakes=self.cake_products or None)

        # add customer to subscribers list, if he approved it
        if data['newsletter']:
            Subscriber.objects.get_or_create(email=data['email'].lower())

        # save into the session to render it on success page
        self.request.session['order_id'] = order.id

        # send email
        order.send_email()

    @atomic
    def save_chocolate(self, order_id, chocolates=None):
        if chocolates is None:
            # break if there is no ordered chocolates
            return
        for choco in chocolates:
            shape_exists = bool(
                'plate' in choco.item and choco.item['plate'].keys() and choco.item['plate']['shape'] != ''
            )
            # save chocolate base
            chocolate = self.chocolate_models['base'].objects.create(
                order_id=order_id,
                quantity=choco.quantity,
                base_id=choco.item['baseId'],
                weight=choco.full_weight,
                price=choco.full_price,
                package_id=choco.item['packageId'],
                label=choco.item['packageText'] or 'MeGusto',
                shape=settings.SHAPE_ALIASES[
                    choco.item['plate']['shape']
                ] if shape_exists else settings.SHAPE_NONE,
                shape_label=choco.item['plate']['text'] if shape_exists else '',
            )
            # save ingredients
            chocolate_ingredients = (
                self.chocolate_models['ingredients'](
                    item_id=chocolate.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in choco.item.get('ingredients', [])
            )
            self.chocolate_models['ingredients'].objects.bulk_create(chocolate_ingredients)

    @atomic
    def save_coffee(self, order_id, coffee=None):
        if coffee is None:
            return

        for c in coffee:
            print c, c.item
            coffee_item = self.coffee_models['base'].objects.create(
                order_id=order_id,
                quantity=c.item['quantity'],
                base_id=c.item['baseId'],
                weight=c.full_weight,
                price=c.full_price,
                package_id=c.item['packageId'],
                label=c.item['packageText'] or 'MeGusto',
                grist=settings.COFFEE_GRIST_ALIASES[
                    c.item.get('grist', 'ground')
                ]
            )
            # save ingredients
            coffee_ingredients = (
                self.coffee_models['ingredients'](
                    item_id=coffee_item.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in c.item.get('ingredients', [])
            )
            self.coffee_models['ingredients'].objects.bulk_create(coffee_ingredients)

    @atomic
    def save_muesli(self, order_id, muesli=None):
        if muesli is None:
            return

        for m in muesli:
            muesli_item = self.muesli_models['base'].objects.create(
                order_id=order_id,
                quantity=m.item['quantity'],
                base_id=m.item['baseId'],
                weight=m.full_weight,
                price=m.full_price,
                package_id=m.item['packageId'],
                label=m.item['packageText'] or 'MeGusto',
            )
            # save ingredients
            muesli_ingredients = (
                self.muesli_models['ingredients'](
                    item_id=muesli_item.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in m.item.get('ingredients', [])
            )
            self.muesli_models['ingredients'].objects.bulk_create(muesli_ingredients)


    @atomic
    def save_macarons(self, order_id, macarons=None):
        if macarons is None:
            return

        for m in macarons:
            macarons_item = self.macarons_models['base'].objects.create(
                order_id=order_id,
                quantity=m.item['quantity'],
                weight=m.full_weight,
                price=m.full_price,
                package_id=m.item['packageId'],
                label=m.item['packageText'] or 'MeGusto',
            )
            # save ingredients
            macarons_ingredients = (
                self.macarons_models['ingredients'](
                    item_id=macarons_item.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in m.item.get('ingredients', [])
            )
            self.macarons_models['ingredients'].objects.bulk_create(macarons_ingredients)

    @atomic
    def save_cake(self, order_id, cakes=None):
        if cakes is None:
            return

        for cake in cakes:
            shape_exists = bool(
                'plate' in cake.item and cake.item['plate'].keys() and cake.item['plate']['shape'] != ''
            )
            cake_item = self.cake_models['base'].objects.create(
                order_id=order_id,
                quantity=cake.item['quantity'],
                base_id=cake.item['baseId'],
                weight=cake.full_weight,
                price=cake.full_price,
                package_id=cake.item['packageId'],
                label=cake.item['packageText'] or 'MeGusto',
                shape=settings.SHAPE_ALIASES[
                    cake.item['plate']['shape']
                ] if shape_exists else settings.SHAPE_NONE,
                shape_label=cake.item['plate']['text'] if shape_exists else '',
            )
            # save ingredients
            cake_ingredients = (
                self.cake_models['ingredients'](
                    item_id=cake_item.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in cake.item.get('ingredients', [])
            )
            self.cake_models['ingredients'].objects.bulk_create(cake_ingredients)
            # save ornamentations
            cake_ornamentations = (
                self.cake_models['ornamentations'](
                    item_id=cake_item.id,
                    ingredient_id=ingr['id'],
                    quantity=ingr['quantity']
                ) for ingr in cake.item.get('ornamentations', [])
            )
            self.cake_models['ornamentations'].objects.bulk_create(cake_ornamentations)


save_order_api = SaveOrderAPI.as_view()
