#! -*- coding: utf-8 -*-
from django import forms


class OrderValidationForm(forms.Form):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    phone = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(required=True)
    date = forms.CharField(required=True)
    time = forms.IntegerField(required=True)
    city = forms.CharField(required=True)
    address = forms.CharField(required=True, widget=forms.Textarea)
    comment = forms.CharField(required=False, widget=forms.Textarea)
    payment_method = forms.IntegerField(required=False)
    newsletter = forms.BooleanField(required=False)
    promo_code = forms.CharField(required=False)

    class Meta:
        exclude = ()
