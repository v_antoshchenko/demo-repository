#! -*- coding: utf-8 -*-
import abc
import itertools

from choco import models as choco_models
from coffee import models as coffee_models
from muesli import models as muesli_models
from macarons import models as macarons_models
from cakes import models as cakes_models


class AbstractHelper(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def quantity(self):
        raise NotImplementedError

    @abc.abstractproperty
    def price(self):
        raise NotImplementedError

    @abc.abstractproperty
    def full_price(self):
        raise NotImplementedError

    @abc.abstractproperty
    def weight(self):
        raise NotImplementedError

    @abc.abstractproperty
    def full_weight(self):
        raise NotImplementedError

    @abc.abstractproperty
    def quantity(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_price(self):
        raise NotImplementedError

    @abc.abstractmethod
    def get_weight(self):
        raise NotImplementedError


class AvailabilityHelper(object):
    # chocolate
    available_chocolate = None
    available_chocolate_ingredients = None
    # coffee
    available_coffee = None
    available_coffee_ingredients = None
    # mues;i
    available_muesli = None
    available_muesli_ingredients = None
    # macarons
    available_macarons = None
    available_macarons_ingredients = None
    # cakes
    available_cake = None
    available_cake_ingredients = None
    available_cake_ornamentations = None

    def __init__(self, data):
        self.generate_available_items(data)

    # FIXME: this is still not optimal, we need to stick to the tuples of dicts, instead of querysets,
    # because of in get_price there new query going to db and that's really BAD
    def generate_available_items(self, data):
        # generate available chocolates
        self.available_chocolate = choco_models.Chocolate.objects.filter(id__in=set(
            itertools.imap(
                lambda x: x['baseId'],
                itertools.ifilter(lambda x: x['productType'] == 'chocolate', data)
            ))).values('id', 'price_100', 'price_1000')
        self.available_chocolate_ingredients = choco_models.ChocolateIngredient.objects.filter(id__in=set(
            itertools.imap(lambda x: x['id'], itertools.chain.from_iterable(
                itertools.imap(
                    lambda x: x.get('ingredients', []), itertools.ifilter(
                        lambda x: x['productType'] == 'chocolate', data
                    )))))).values('id', 'price_100', 'price_1000', 'weight_100', 'weight_1000')

        # generate available coffee
        self.available_coffee = coffee_models.Coffee.objects.filter(id__in=set(
            itertools.imap(
                lambda x: x['baseId'],
                itertools.ifilter(lambda x: x['productType'] == 'coffee', data)
            ))).values('id', 'price_150', 'price_300')
        self.available_coffee_ingredients = coffee_models.CoffeeIngredient.objects.filter(id__in=set(
            itertools.imap(lambda x: x['id'], itertools.chain.from_iterable(
                itertools.imap(
                    lambda x: x.get('ingredients', []), itertools.ifilter(
                        lambda x: x['productType'] == 'coffee', data
                    )))))).values('id', 'price_150', 'price_300', 'weight_150', 'weight_300')

        # generate available muesli
        self.available_muesli = muesli_models.Muesli.objects.filter(id__in=set(
            itertools.imap(
                lambda x: x['baseId'],
                itertools.ifilter(lambda x: x['productType'] == 'muesli', data)
            ))).values('id', 'price', 'weight')
        self.available_muesli_ingredients = muesli_models.MuesliIngredient.objects.filter(id__in=set(
            itertools.imap(lambda x: x['id'], itertools.chain.from_iterable(
                itertools.imap(
                    lambda x: x.get('ingredients', []), itertools.ifilter(
                        lambda x: x['productType'] == 'muesli', data
                    )))))).values('id', 'price', 'weight')

        # generate available macarons
        self.available_macarons = macarons_models.MacaronsConfiguration.get_solo()
        self.available_macarons_ingredients = macarons_models.Macarons.objects.none()  # no need for this

        # generate available cakes
        self.available_cake = cakes_models.Cake.objects.filter(id__in=set(
            itertools.imap(
                lambda x: x['baseId'],
                itertools.ifilter(lambda x: x['productType'] == 'cake', data)
            ))).values('id', 'price_1', 'price_2')
        self.available_cake_ingredients = cakes_models.CakeIngredient.objects.filter(id__in=set(
            itertools.imap(lambda x: x['id'], itertools.chain.from_iterable(
                itertools.imap(
                    lambda x: x.get('ingredients', []), itertools.ifilter(
                        lambda x: x['productType'] == 'cake', data
                    )))))).values('id', 'price_1', 'price_2', 'weight_1', 'weight_2')
        self.available_cake_ornamentations = cakes_models.CakeOrnamentation.objects.filter(id__in=set(
            itertools.imap(lambda x: x['id'], itertools.chain.from_iterable(
                itertools.imap(
                    lambda x: x.get('ornamentations', []), itertools.ifilter(
                        lambda x: x['productType'] == 'cake', data
                    )))))).values('id', 'price_1', 'price_2', 'weight_1', 'weight_2')


class BaseHelper(object):
    proxy_helper = None

    def __init__(self, item):
        if not isinstance(item, dict):
            raise TypeError('item must be dict instance')

        self.item = item

        if isinstance(self.proxy_helper, AvailabilityHelper):
            for attr in filter(lambda x: x.startswith('available'), self.proxy_helper.__class__.__dict__):
                setattr(self, attr, getattr(self.proxy_helper, attr))
        else:
            raise AttributeError('proxy_helper must be AvailabilityHelper instance')

        self._weight = None
        self._price = None
        self._quantity = None

    @property
    def quantity(self):
        if self._quantity is None:
            self._quantity = self.item.get('quantity', 1)
        return self._quantity

    @property
    def price(self):
        if self._price is None:
            self._price = self.get_price()
        return self._price

    @property
    def weight(self):
        if self._weight is None:
            self._weight = self.get_weight()
        return self._weight

    @property
    def full_price(self):
        return self.price * self.quantity

    @property
    def full_weight(self):
        return self.weight * self.quantity


class ChocolateHelper(BaseHelper, AbstractHelper):
    available_chocolate = None
    available_chocolate_ingredients = None

    def __init__(self, chocolate):
        super(ChocolateHelper, self).__init__(chocolate)

    def get_price(self):
        price = self.available_chocolate.get(id=self.item['baseId'])[
            'price_1000' if self.item['weight'] == 1 else 'price_100'
        ]
        price += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ingredients', [])),
                           list(self.available_chocolate_ingredients.filter(
                               id__in=(i['id'] for i in self.item.get('ingredients', []))
                           ).values_list('price_1000' if self.item['weight'] == 1 else 'price_100', flat=True))
            )
        ) / 100.0  # because in cents
        return price

    def get_weight(self):
        weight = 1000 if self.item['weight'] == 1 else self.item['weight']
        weight += sum(list(
            self.available_chocolate_ingredients.filter(
                id__in=(i['id'] for i in self.item.get('ingredients', []))
            ).values_list('weight_1000' if self.item['weight'] == 1 else 'weight_100', flat=True)))
        return weight


class CoffeeHelper(BaseHelper, AbstractHelper):
    available_coffee = None
    available_coffee_ingredients = None

    def __init__(self, coffee):
        super(CoffeeHelper, self).__init__(coffee)

    def get_price(self):
        price = self.available_coffee.get(id=self.item['baseId'])[
            'price_150' if self.item['weight'] == 150 else 'price_300'
        ]
        price += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ingredients', [])),
                           list(self.available_coffee_ingredients.filter(
                               id__in=(i['id'] for i in self.item.get('ingredients', []))
                           ).values_list('price_150' if self.item['weight'] == 150 else 'price_300', flat=True))
            )
        ) / 100.0  # because in cents
        return price

    def get_weight(self):
        weight = self.item['weight']
        weight += sum(list(
            self.available_coffee_ingredients.filter(
                id__in=(i['id'] for i in self.item.get('ingredients', []))
            ).values_list('weight_150' if self.item['weight'] == 150 else 'weight_300', flat=True)))
        return weight


class MuesliHelper(BaseHelper, AbstractHelper):
    available_muesli = None
    available_muesli_ingredients = None

    def __init__(self, muesli):
        super(MuesliHelper, self).__init__(muesli)

    def get_price(self):
        price = self.available_muesli.get(id=self.item['baseId'])['price']
        price += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ingredients', [])),
                           list(self.available_muesli_ingredients.filter(
                               id__in=(i['id'] for i in self.item.get('ingredients', []))
                           ).values_list('price', flat=True))
            )
        ) / 100.0  # because in cents
        return price

    def get_weight(self):
        weight = self.item['weight']
        weight += sum(list(
            self.available_muesli_ingredients.filter(
                id__in=(i['id'] for i in self.item.get('ingredients', []))
            ).values_list('weight', flat=True)))
        return weight


class MacaronsHelper(BaseHelper, AbstractHelper):
    available_macarons = None

    def __init__(self, macarons):
        super(MacaronsHelper, self).__init__(macarons)

    def get_price(self):
        return self.available_macarons.price

    def get_weight(self):
        return self.available_macarons.weight


class CakesHelper(BaseHelper, AbstractHelper):
    available_cake = None
    available_cake_ingredients = None
    available_cake_ornamentations = None

    def __init__(self, cake):
        super(CakesHelper, self).__init__(cake)

    def get_price(self):
        price = self.available_cake.get(id=self.item['baseId'])[
            'price_1' if self.item['weight'] == 1 else 'price_2'
        ]
        price += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ingredients', [])),
                           list(self.available_cake_ingredients.filter(
                               id__in=(i['id'] for i in self.item.get('ingredients', []))
                           ).values_list('price_1' if self.item['weight'] == 1 else 'price_2', flat=True))
            )
        ) / 100.0  # because in cents
        price += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ornamentations', [])),
                           list(self.available_cake_ornamentations.filter(
                               id__in=(i['id'] for i in self.item.get('ornamentations', []))
                           ).values_list('price_1' if self.item['weight'] == 1 else 'price_2', flat=True))
            )
        ) / 100.0  # because in cents
        return price

    def get_weight(self):
        weight = self.item['weight']
        weight += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ingredients', [])),
                           list(self.available_cake_ingredients.filter(
                               id__in=(i['id'] for i in self.item.get('ingredients', []))
                           ).values_list('weight_1' if self.item['weight'] == 1 else 'weight_2', flat=True))
            )
        ) / 100.0  # because in cents
        weight += sum(
            itertools.imap(lambda x, y: x * y,
                           (i['quantity'] for i in self.item.get('ornamentations', [])),
                           list(self.available_cake_ornamentations.filter(
                               id__in=(i['id'] for i in self.item.get('ornamentations', []))
                           ).values_list('weight_1' if self.item['weight'] == 1 else 'weight_2', flat=True))
            )
        ) / 100.0  # because in cents
        return weight

# registering subclasses
AbstractHelper.register(ChocolateHelper)
AbstractHelper.register(CoffeeHelper)
AbstractHelper.register(MuesliHelper)
AbstractHelper.register(MacaronsHelper)
AbstractHelper.register(CakesHelper)
