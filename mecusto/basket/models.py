from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template.loader import render_to_string
from django.utils import translation
from hvad.models import TranslatableModel, TranslatedFields
from django.utils.translation import ugettext_lazy as _
from django.conf import settings as mecusto_settings
from itertools import chain

from mecusto.utils.models import PositionMixin
import settings


class Package(PositionMixin, TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=15)
    )
    product = models.CharField(verbose_name=_('product'), max_length=25, choices=(
        (settings.PACKAGE_CHOCOLATE, _(settings.PACKAGE_CHOCOLATE)),
        (settings.PACKAGE_COFFEE, _(settings.PACKAGE_COFFEE)),
        (settings.PACKAGE_MUESLI, _(settings.PACKAGE_MUESLI)),
        (settings.PACKAGE_MACARONS, _(settings.PACKAGE_MACARONS)),
        (settings.PACKAGE_CAKE, _(settings.PACKAGE_CAKE)),
    ))

    class Meta:
        ordering = 'position',
        verbose_name = _('package')
        verbose_name_plural = _('packages')

    def __unicode__(self):
        return self.safe_translation_getter('title', default=getattr(self, 'title'))


class City(PositionMixin, TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=35)
    )
    delivery_price = models.FloatField(verbose_name=_('delivery price'), default=0.0)

    class Meta:
        ordering = 'position',
        verbose_name = _('city')
        verbose_name_plural = _('cities')

    def __unicode__(self):
        return self.safe_translation_getter('title', default=getattr(self, 'title'))


class Order(models.Model):
    original_cost = models.FloatField(
        verbose_name=_('original order cost'), help_text=_('Order cost without discount')
    )
    cost = models.FloatField(verbose_name=_('order cost'), help_text=_('Order cost with discount'))
    delivery_cost = models.FloatField(verbose_name=_('delivery cost'))
    first_name = models.CharField(verbose_name=_('first name'), max_length=75)
    last_name = models.CharField(verbose_name=_('last name'), max_length=75)
    phone = models.CharField(verbose_name=_('phone'), max_length=20)
    email = models.EmailField(verbose_name=_('email'))
    order_date = models.DateTimeField(verbose_name=_('order date'), auto_now_add=True)
    delivery_date = models.DateField(verbose_name=_('delivery date'))
    delivery_time = models.PositiveSmallIntegerField(
        verbose_name=_('delivery time'), choices=settings.ORDER_TIME_RANGE_CHOICES
    )
    city = models.ForeignKey(verbose_name=_('delivery city'), to='City')
    address = models.TextField(verbose_name=_('delivery address'))
    customer_comment = models.TextField(verbose_name=_('client comment'), blank=True, null=True)
    manager_comment = models.TextField(verbose_name=_('manger comment'), blank=True, null=True)
    promo_code = models.CharField(verbose_name=_('promo code'), max_length=50)
    status = models.PositiveSmallIntegerField(verbose_name=_('order status'), choices=(
        (settings.ORDER_STATUS_ACCEPTED, _('order accepted')),
        (settings.ORDER_STATUS_SENT_TO_PRODUCTION, _('order sent to production')),
        (settings.ORDER_STATUS_SENT_TO_CUSTOMER, _('order sent to customer')),
        (settings.ORDER_STATUS_DELIVERED, _('order delivered')),
        (settings.ORDER_STATUS_IGNORED, _('order ignored'))
    ))
    customer_language = models.CharField(
        verbose_name=_('customer language'), max_length=5, choices=mecusto_settings.LANGUAGES
    )
    mail_delivered = models.BooleanField(verbose_name=_('mail is delivered'), default=False)
    payment_method = models.PositiveSmallIntegerField(verbose_name=_('payment method'), choices=(
        (0, _('courier')),
        (1, _('bill')),
        (2, _('online')),
    ))

    def __unicode__(self):
        return unicode(self.id)

    def get_order_items(self):
        return chain(
            self.chocolate_set.all(),
            self.coffee_set.all(),
            self.muesli_set.all(),
            self.macarons_set.all(),
            self.cake_set.all(),
        )

    def get_customer_full_name(self):
        return u'{0} {1}'.format(self.first_name, self.last_name)

    def send_email(self):
        translation.activate(self.customer_language)
        message = render_to_string('mail/order-info.html', {'order': self})
        msg = EmailMultiAlternatives(
            u'MeGusto # %s' % str(self.id), u'', 'info@megusto.ee', [self.email], cc=['info@megusto.ee']
        )
        msg.attach_alternative(message, "text/html")
        delivered = msg.send()
        if delivered:
            self.mail_delivered = True
            self.save()
        return delivered

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')


class OrderItem(models.Model):
    order = models.ForeignKey(verbose_name=_('order'), to='Order')
    quantity = models.PositiveSmallIntegerField(verbose_name=_('quantity'), default=1)
    price = models.FloatField(verbose_name=_('price'), help_text=_(
        'price without discount for %(b_tag_open)squantity%(b_tag_close)s items' % dict(
            b_tag_open='<b>', b_tag_close='</b>'
        )
    ))
    weight = models.FloatField(verbose_name=_('weight'))
    package = models.ForeignKey(verbose_name=_('package'), to='Package')
    label = models.CharField(verbose_name=_('package label'), default='MeGusto', max_length=20)

    def __unicode__(self):
        return unicode(self.id)

    def get_inner_type(self):
        return self.__class__.__name__.lower()

    def get_price_for_single_item(self):
        return self.price / self.quantity

    def get_ingredients(self):
        raise NotImplementedError

    class Meta:
        abstract = True


class Chocolate(OrderItem):
    base = models.ForeignKey(verbose_name=_('chocolate'), to='choco.Chocolate')
    shape = models.PositiveSmallIntegerField(verbose_name=_('shape type'), choices=(
        (settings.SHAPE_NONE, _('no shape')),
        (settings.SHAPE_HEART, _('heart')),
        (settings.SHAPE_BOX, _('box')),
        (settings.SHAPE_STAR, _('star')),
    ))
    shape_label = models.CharField(verbose_name=_('shape label'), max_length=70, blank=True, null=True)

    def get_ingredients(self):
        return self.chocolateingredient_set.only('ingredient')

    class Meta:
        verbose_name = _('Ordered chocolate')
        verbose_name_plural = _('Ordered chocolates')


class ChocolateIngredient(models.Model):
    item = models.ForeignKey(verbose_name=_('chocolate'), to='Chocolate')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='choco.ChocolateIngredient')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_3
    )

    class Meta:
        verbose_name = _('Ordered chocolate ingredient')
        verbose_name_plural = _('Ordered chocolate ingredient')


class Coffee(OrderItem):
    base = models.ForeignKey(verbose_name=_('coffee'), to='coffee.Coffee')
    grist = models.PositiveSmallIntegerField(verbose_name=_('grist'), default=settings.COFFEE_GRIST_GROUND, choices=(
        (settings.COFFEE_GRIST_GROUND, _('ground')),
        (settings.COFFEE_GRIST_BEANS, _('beans')),
    ))

    def get_ingredients(self):
        return self.coffeeingredient_set.only('ingredient')

    class Meta:
        verbose_name = _('Ordered coffee')
        verbose_name_plural = _('Ordered coffee')


class CoffeeIngredient(models.Model):
    item = models.ForeignKey(verbose_name=_('coffee'), to='Coffee')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='coffee.CoffeeIngredient')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_3
    )

    class Meta:
        verbose_name = _('Ordered coffee ingredient')
        verbose_name_plural = _('Ordered coffee ingredient')


class Muesli(OrderItem):
    base = models.ForeignKey(verbose_name=_('muesli'), to='muesli.Muesli')

    def get_ingredients(self):
        return self.muesliingredient_set.only('ingredient')

    class Meta:
        verbose_name = _('Ordered muesli')
        verbose_name_plural = _('Ordered muesli')


class MuesliIngredient(models.Model):
    item = models.ForeignKey(verbose_name=_('muesli'), to='Muesli')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='muesli.MuesliIngredient')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_3
    )

    class Meta:
        verbose_name = _('Ordered muesli ingredient')
        verbose_name_plural = _('Ordered muesli ingredient')


class Macarons(OrderItem):

    def get_ingredients(self):
        return self.macaronsingredient_set.only('ingredient')

    class Meta:
        verbose_name = _('Ordered macarons')
        verbose_name_plural = _('Ordered macarons')


class MacaronsIngredient(models.Model):
    item = models.ForeignKey(verbose_name=_('macarons'), to='Macarons')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='macarons.Macarons')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_7
    )

    class Meta:
        verbose_name = _('Ordered macarons ingredient')
        verbose_name_plural = _('Ordered macarons ingredient')


class Cake(OrderItem):
    base = models.ForeignKey(verbose_name=_('cake'), to='cakes.Cake')
    shape = models.PositiveSmallIntegerField(verbose_name=_('shape type'), choices=(
        (settings.SHAPE_NONE, _('no shape')),
        (settings.SHAPE_HEART, _('heart')),
        (settings.SHAPE_BOX, _('box')),
        (settings.SHAPE_STAR, _('star')),
    ))
    shape_label = models.CharField(verbose_name=_('shape label'), max_length=70, blank=True, null=True)

    def get_ingredients(self):
        return self.cakeingredient_set.only('ingredient')

    def get_ornamentations(self):
        return self.cakeornamentation_set.only('ingredient')

    class Meta:
        verbose_name = _('Ordered cake')
        verbose_name_plural = _('Ordered cakes')


class CakeIngredient(models.Model):
    item = models.ForeignKey(verbose_name=_('cake'), to='Cake')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='cakes.CakeIngredient')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_3
    )

    class Meta:
        verbose_name = _('Ordered cake ingredient')
        verbose_name_plural = _('Ordered cake ingredient')


class CakeOrnamentation(models.Model):
    item = models.ForeignKey(verbose_name=_('cake'), to='Cake')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='cakes.CakeOrnamentation')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'), default=1, choices=settings.INGREDIENTS_QUANTITY_CHOICES_3
    )

    class Meta:
        verbose_name = _('Ordered cake ornamentation')
        verbose_name_plural = _('Ordered cake ornamentations')
