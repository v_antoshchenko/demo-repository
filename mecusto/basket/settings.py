#! -*- coding: utf-8 -*-
from django.utils.safestring import mark_safe

# order delivery time choices
ORDER_DELIVERY_TIME_TO_12 = 0
ORDER_DELIVERY_TIME_TO_14 = 1
ORDER_DELIVERY_TIME_TO_18 = 2
ORDER_DELIVERY_TIME_TO_21 = 3

# order status choices
ORDER_STATUS_ACCEPTED = 0
ORDER_STATUS_SENT_TO_PRODUCTION = 1
ORDER_STATUS_SENT_TO_CUSTOMER = 2
ORDER_STATUS_DELIVERED = 3
ORDER_STATUS_IGNORED = 4

# order payment method
ORDER_PAYMENT_COURIER = 0
ORDER_PAYMENT_BILL = 1
ORDER_PAYMENT_ONLINE = 2


# order items shapes (if they're support this)
SHAPE_NONE = 0
SHAPE_HEART = 1
SHAPE_BOX = 2
SHAPE_STAR = 3
SHAPE_CIRCLE = 4

SHAPE_ALIASES = {
    'heart': SHAPE_HEART,
    'rect': SHAPE_BOX,
    'star': SHAPE_STAR,
    'circle': SHAPE_CIRCLE
}

COFFEE_GRIST_GROUND = 0
COFFEE_GRIST_BEANS = 1

COFFEE_GRIST_ALIASES = {
    'ground': COFFEE_GRIST_GROUND,
    'beans': COFFEE_GRIST_BEANS
}

PACKAGE_CHOCOLATE = 'chocolate'
PACKAGE_COFFEE = 'coffee'
PACKAGE_MUESLI = 'muesli'
PACKAGE_CAKE = 'cake'
PACKAGE_MACARONS = 'macarons'

INGREDIENTS_QUANTITY_CHOICES_3 = (
    (1, 'x1'),
    (2, 'x2'),
    (3, 'x3'),
)

INGREDIENTS_QUANTITY_CHOICES_7 = (
    (1, 'x1'),
    (2, 'x2'),
    (3, 'x3'),
    (4, 'x4'),
    (5, 'x5'),
    (6, 'x6'),
    (7, 'x7'),
)


ORDER_TIME_RANGE_CHOICES = (
    (ORDER_DELIVERY_TIME_TO_12, mark_safe('10:00 &#8211; 12:00')),
    (ORDER_DELIVERY_TIME_TO_14, mark_safe('10:00 &#8211; 14:00')),
    (ORDER_DELIVERY_TIME_TO_18, mark_safe('15:00 &#8211; 18:00')),
    (ORDER_DELIVERY_TIME_TO_21, mark_safe('17:00 &#8211; 21:00'))
)

