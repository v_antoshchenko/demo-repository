from django.conf.urls import patterns, url
from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('basket.views',
   url(r'^$', 'basket_page', name='basket-page'),
   url(_(r'^/order$'), 'order_page', name='order-page'),
   url(_(r'^/success$'), 'success_page', name='success-page'),
)
