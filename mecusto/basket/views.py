from django.http.response import Http404
from django.views.generic import TemplateView

from basket.models import Package, City, Order
import settings as basket_settings
from datetime import datetime


class BasketPage(TemplateView):
    template_name = 'basket/basket.html'

    def get_context_data(self, **kwargs):
        kwargs.update(**self.get_packages())
        return kwargs

    @staticmethod
    def get_packages():
        all_packages = list(Package.objects.language().values('id', 'title', 'product'))
        return {
            '%s_packages' % basket_settings.PACKAGE_CHOCOLATE: [
                i for i in all_packages if i['product'] == basket_settings.PACKAGE_CHOCOLATE
            ],
            '%s_packages' % basket_settings.PACKAGE_COFFEE: [
                i for i in all_packages if i['product'] == basket_settings.PACKAGE_COFFEE
            ],
            '%s_packages' % basket_settings.PACKAGE_MUESLI: [
                i for i in all_packages if i['product'] == basket_settings.PACKAGE_MUESLI
            ],
            '%s_packages' % basket_settings.PACKAGE_CAKE: [
                i for i in all_packages if i['product'] == basket_settings.PACKAGE_CAKE
            ],
            '%s_packages' % basket_settings.PACKAGE_MACARONS: [
                i for i in all_packages if i['product'] == basket_settings.PACKAGE_MACARONS
            ]
        }


class OrderPage(TemplateView):
    template_name = 'basket/order.html'

    def get(self, request, *args, **kwargs):
        return super(OrderPage, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'time_range': basket_settings.ORDER_TIME_RANGE_CHOICES,
            'cities': City.objects.all(),
            'min_date': '+1d' if datetime.now().hour < 11 else '+2d'
        })
        return super(OrderPage, self).get_context_data(**kwargs)


class SuccessPage(TemplateView):
    template_name = 'basket/success.html'

    def get(self, request, *args, **kwargs):
        if 'order_id' not in self.request.session:
            raise Http404()
        try:
            self.order = Order.objects.get(id=self.request.session['order_id'])
        except Order.DoesNotExist:
            raise Http404()
        del self.request.session['order_id']
        self.request.session.modified = True
        return super(SuccessPage, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update({'order': self.order})
        return kwargs

basket_page = BasketPage.as_view()
order_page = OrderPage.as_view()
success_page = SuccessPage.as_view()
