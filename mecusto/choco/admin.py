from django.contrib import admin
from adminsortable.admin import SortableAdminMixin
from hvad.admin import TranslatableAdmin
from django.utils.translation import ugettext_lazy as _

from mecusto.admin import mecusto_admin
import models


@admin.register(models.ChocolateMessage, site=mecusto_admin)
class ChocolateMessageAdmin(SortableAdminMixin, TranslatableAdmin):
    pass


@admin.register(models.Chocolate, site=mecusto_admin)
class ChocolateAdmin(SortableAdminMixin, TranslatableAdmin):
    fieldsets = (
        (None, {'fields': ('title', 'image', 'chocolate_type', 'slug')}),
        (_('Price information'), {'fields': (('price_100', 'price_1000'),)}),
        (_('Nutrition information per 100 g'), {'fields': ('energy_value', 'fats', 'carbohydrates')}),
        (_('SEO'), {'fields': ('seo_text',)})
    )


@admin.register(models.ChocolateIngredient, site=mecusto_admin)
class ChocolateIngredientAdmin(SortableAdminMixin, TranslatableAdmin):
    fieldsets = (
        (None, {'fields': (
            'group', 'title', ('price_100', 'price_1000'), ('weight_100', 'weight_1000'), 'image', 'description',
            'is_available'
        )}),
        (_('Nutrition information per 100 g'), {'fields': ('energy_value', 'fats', 'carbohydrates')}),
    )


@admin.register(models.ChocolateIngredientGroup, site=mecusto_admin)
class ChocolateIngredientGroupAdmin(SortableAdminMixin, TranslatableAdmin):
    fields = 'title',


# class ChocolateIngredientRecommendationAdmin(admin.TabularInline):
#     model = models.ChocolateRecommendationIngredient
#     extra = 1
#
#
# @admin.register(models.ChocolateRecommendation, site=mecusto_admin)
# class ChocolateRecommendationAdmin(SortableAdminMixin, TranslatableAdmin):
#     fieldsets = (
#         (None, {'fields': ('base', 'title', 'sub_title', 'description', 'image', 'slug', 'weight')}),
#     )
#     inlines = ChocolateIngredientRecommendationAdmin,
