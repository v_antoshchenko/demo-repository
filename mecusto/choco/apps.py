#! -*- coding: utf-8 -*-
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ChocolateConfig(AppConfig):
    name = 'choco'
    verbose_name = _('Chocolate')
