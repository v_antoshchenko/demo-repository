#! -*- coding: utf-8 -*-
import json

from django.core.validators import MinValueValidator
from django.db import models
from django.utils.functional import cached_property
from hvad.models import TranslatableModel, TranslatedFields
from django.utils.translation import ugettext_lazy as _
from hvad.utils import get_translation_aware_manager
from django.conf import settings

from mecusto.utils import ProductMixin, EnergyValueMixin, PositionMixin
from mecusto.utils.models import AvailabilityMixin


class Chocolate(ProductMixin, EnergyValueMixin, PositionMixin, TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=50, unique=True),
        slug=models.SlugField(verbose_name=_('slug'), unique=True),
        description=models.CharField(verbose_name=_('description'), max_length=500, blank=True, null=True),
        seo_title=models.CharField(max_length=300, verbose_name=_('SEO title'), default='MeGusto'),
        seo_description=models.TextField(verbose_name=_('SEO description'), default='MeGusto'),
        seo_keywords=models.TextField(verbose_name=_('SEO keywords'), default='MeGusto'),
        seo_text=models.TextField(verbose_name=_('SEO text'), default='MeGusto')
    )
    chocolate_type = models.PositiveSmallIntegerField(verbose_name=_('chocolate type'), default=0, choices=(
        (0, _('black chocolate')),
        (1, _('milk chocolate')),
        (2, _('sugarless chocolate')),
        (3, _('white chocolate')),
    ))
    price_100 = models.FloatField(
        verbose_name=_('price for 100 g'), default=0, validators=[MinValueValidator(0)],
        help_text=_('in euro')
    )
    price_1000 = models.FloatField(
        verbose_name=_('price for 1000 g'), default=0, validators=[MinValueValidator(0)],
        help_text=_('in euro')
    )
    image = models.ImageField(verbose_name=_('image'), upload_to='chocolate')

    class Meta:
        ordering = 'position',
        verbose_name = _('chocolate')
        verbose_name_plural = _('chocolates')


class ChocolateIngredientGroup(TranslatableModel, PositionMixin):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=50, unique=True)
    )

    class Meta:
        ordering = 'position',
        verbose_name = _('ingredient group')
        verbose_name_plural = _('ingredient groups')

    def __unicode__(self):
        return self.safe_translation_getter('title', default=getattr(self, 'title'))

    @cached_property
    def ingredients(self):
        return ChocolateIngredient.objects.language().filter(group_id=self.id, is_available=True)


class ChocolateIngredient(TranslatableModel, EnergyValueMixin, PositionMixin, AvailabilityMixin):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=50, unique=True),
        description=models.CharField(verbose_name=_('description'), max_length=500, blank=True, null=True)
    )
    group = models.ForeignKey(verbose_name=_('ingredient group'), to='ChocolateIngredientGroup')
    price_100 = models.FloatField(
        verbose_name=_('price 100 g'), default=0.0, validators=[MinValueValidator(0)],
        help_text=_('in cents')
    )
    price_1000 = models.FloatField(
        verbose_name=_('price 1 kg'), default=0.0, validators=[MinValueValidator(0)],
        help_text=_('in cents')
    )
    weight_100 = models.FloatField(
        verbose_name=_('weight 100 g'), default=0, validators=[MinValueValidator(0)],
        help_text=_('in grams')
    )
    weight_1000 = models.FloatField(
        verbose_name=_('weight 1 kg'), default=0, validators=[MinValueValidator(0)],
        help_text=_('in grams')
    )
    image = models.ImageField(verbose_name=_('image'), upload_to='chocolate/ingredients')

    class Meta:
        ordering = 'position',
        verbose_name = _('ingredient')
        verbose_name_plural = _('ingredients')

    def __unicode__(self):
        return self.safe_translation_getter('title', default=getattr(self, 'title'))


class ChocolateRecommendation(TranslatableModel, PositionMixin):
    translations = TranslatedFields(
        title=models.CharField(verbose_name=_('title'), max_length=50, unique=True),
        sub_title=models.CharField(verbose_name=_('sub title'), max_length=150),
        slug=models.SlugField(verbose_name=_('slug'), unique=True),
        description=models.CharField(verbose_name=_('description'), max_length=500, blank=True, null=True)
    )
    base = models.ForeignKey(verbose_name=_('base'), to='Chocolate')
    image = models.ImageField(verbose_name=_('image'), upload_to='chocolate/recommendations')
    weight = models.PositiveSmallIntegerField(
        verbose_name=_('weight'),
        max_length=10,
        default=100,
        choices=(
            (100, _('100 g')),
            (1, _('1 kg')),
        )
    )

    class Meta:
        ordering = 'position',
        verbose_name = _('recommended chocolate')
        verbose_name_plural = _('recommended chocolate')

    def __unicode__(self):
        return self.safe_translation_getter('title', default=getattr(self, 'title'))

    @cached_property
    def ingredients(self):
        return get_translation_aware_manager(ChocolateRecommendationIngredient).filter(recommendation_id=self.id)

    @cached_property
    def get_ingredients_json(self):
        table_name = self.ingredients.model.ingredient.field.related.parent_model._meta.db_table
        results = list(self.ingredients.values('ingredient__id').extra(select={
            'img': '"%(table_name)s"."image"' % {'table_name': table_name},
            'energy': '"%(table_name)s"."energy_value"' % {'table_name': table_name},
            'carbohydrates': '"%(table_name)s"."carbohydrates"' % {'table_name': table_name},
            'fats': '"%(table_name)s"."fats"' % {'table_name': table_name},
        }).values('id', 'img', 'quantity', 'energy', 'carbohydrates', 'fats'))

        for item in results:
            item['img'] = settings.MEDIA_URL + item['img']

        # title in translations, but we need it
        for ingredient, res_elem in zip(self.ingredients, results):
            res_elem['title'] = ingredient.ingredient.title
            if self.weight == 1:
                res_elem['weight'] = ingredient.ingredient.weight_1000
                res_elem['price'] = ingredient.ingredient.price_1000
            else:
                res_elem['weight'] = ingredient.ingredient.weight_100
                res_elem['price'] = ingredient.ingredient.price_100

        return json.dumps(results)

    @cached_property
    def get_price(self):
        price = float(0)
        if self.weight is 100:
            price += self.base.price_100
        else:
            price += self.base.price_1000

        return price

    @cached_property
    def get_weight(self):
        return self.weight


class ChocolateRecommendationIngredient(models.Model):
    recommendation = models.ForeignKey(verbose_name=_('recommendation'), to='ChocolateRecommendation')
    ingredient = models.ForeignKey(verbose_name=_('ingredient'), to='ChocolateIngredient')
    quantity = models.PositiveSmallIntegerField(
        verbose_name=_('quantity'),
        default=1,
        choices=(
            (1, 'x1'),
            (2, 'x2'),
            (3, 'x3'),
        )
    )

    class Meta:
        verbose_name = _('recommendation ingredient')
        verbose_name_plural = _('recommendation ingredient')

    def __unicode__(self):
        return u'{0}'.format(self.ingredient)


class ChocolateMessage(TranslatableModel, PositionMixin):
    translations = TranslatedFields(
        message=models.CharField(max_length=180, help_text=_('180 characters max'))
    )

    class Meta:
        ordering = 'position',
        verbose_name = _('message for chocolate pages')
        verbose_name_plural = _('messages for chocolate pages')

    def __unicode__(self):
        return self.safe_translation_getter('message', default=getattr(self, 'message'))
