from django import template

register = template.Library()


@register.filter('get_bg_class')
def do_bg_class(choco_instance):
    if choco_instance.chocolate_type == 0:
        return ' black-choco'
    elif choco_instance.chocolate_type == 1:
        return ' milk-choco'
    elif choco_instance.chocolate_type == 3:
        return ' white-choco'
    else:
        return ''
