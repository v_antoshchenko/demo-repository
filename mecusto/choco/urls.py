from django.conf.urls import patterns, url

urlpatterns = patterns('choco.views',
   url(r'^$', 'index_page', name='index-page'),
   url(r'^/(?P<slug>[\w\-]+)$', 'constructor_page', name='constructor-page'),
   url(r'^/r/(?P<pk>[\w\-]+)$', 'redirect_to_constructor', name='redirect-to-constructor'),
)
