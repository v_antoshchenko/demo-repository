from django.core.urlresolvers import reverse_lazy
from django.utils.translation import get_language
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from common.models import ChocolateIndexPageText

from choco.models import Chocolate, ChocolateRecommendation, ChocolateMessage, ChocolateIngredientGroup


class IndexPage(ListView):
    template_name = 'choco/index.html'
    model = Chocolate
    context_object_name = 'chocolates'

    def get_context_data(self, **kwargs):
        kwargs.update({
            'recommendations': ChocolateRecommendation.objects.all()[:10],
            'seo_text': ChocolateIndexPageText.get_solo()
        })
        return super(IndexPage, self).get_context_data(**kwargs)


class ConstructorPage(DetailView):
    template_name = 'choco/constructor.html'
    model = Chocolate
    context_object_name = 'chocolate'

    def get_queryset(self):
        if self.queryset is None:
            return self.model.objects.language(get_language()).all()
        return self.queryset._clone()

    def get_context_data(self, **kwargs):
        kwargs.update({
            'notifications': ChocolateMessage.objects.all(),
            'groups': ChocolateIngredientGroup.objects.all()
        })
        return super(ConstructorPage, self).get_context_data(**kwargs)


class RedirectToConstructor(RedirectView):
    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        chocolate = Chocolate.objects.get(**kwargs)
        self.url = reverse_lazy('choco:constructor-page', kwargs={'slug': chocolate.slug})
        return super(RedirectToConstructor, self).get_redirect_url(*args, **kwargs)


index_page = IndexPage.as_view()
constructor_page = ConstructorPage.as_view()
redirect_to_constructor = RedirectToConstructor.as_view()
