#! -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as UA
from hvad.admin import TranslatableAdmin
from solo.admin import SingletonModelAdmin
from mecusto.admin import mecusto_admin
import models


@admin.register(User, site=mecusto_admin)
class UserAdmin(UA):
    pass


@admin.register(models.AvailabilityConfiguration, site=mecusto_admin)
class AvailabilityConfigurationAdmin(SingletonModelAdmin):

    def get_list_display(self, request):
        return (i for i in self.model._meta.get_all_field_names() if i != 'id')


@admin.register(models.Subscriber, site=mecusto_admin)
class SubscriberAdmin(admin.ModelAdmin):
    pass


@admin.register(models.IndexPageLeftBlockText, site=mecusto_admin)
class IndexPageLeftBlockTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.IndexPageDescription, site=mecusto_admin)
class IndexPageDescriptionAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.IndexPageTabsManagement, site=mecusto_admin)
class IndexPageTabsManagementAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.ChocolateIndexPageText, site=mecusto_admin)
class ChocolateIndexPageTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.CoffeeIndexPageText, site=mecusto_admin)
class CoffeeIndexPageTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.MuesliIndexPageText, site=mecusto_admin)
class MuesliIndexPageTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.CakeIndexPageText, site=mecusto_admin)
class CakeIndexPageTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass


@admin.register(models.MacaronsIndexPageText, site=mecusto_admin)
class MacaronsIndexPageTextAdmin(TranslatableAdmin, SingletonModelAdmin):
    pass
