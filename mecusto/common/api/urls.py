from django.conf.urls import patterns, url

urlpatterns = patterns('common.api.views',
   url(r'^/save-subscriber$', 'save_subscriber_api', name='save-subscriber'),
)
