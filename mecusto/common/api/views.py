from django.views.generic.edit import FormView
from common.forms import SubscriberForm
from django.http.response import JsonResponse
from common.models import Subscriber


class SaveSubscriber(FormView):
    form_class = SubscriberForm

    def form_invalid(self, form):
        return JsonResponse({'errors': form.errors}, status=400)

    def form_valid(self, form):
        Subscriber.objects.get_or_create(email=form.cleaned_data['email'])
        return JsonResponse({'status': 'OK'})


save_subscriber_api = SaveSubscriber.as_view()
