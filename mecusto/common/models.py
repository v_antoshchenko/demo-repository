#! -*- coding: utf-8 -*-
from django.db import models
from hvad.models import TranslatableModel, TranslatedFields
from solo.models import SingletonModel
from django.utils.translation import ugettext_lazy as _


class AvailabilityConfiguration(SingletonModel):
    chocolate_is_available = models.BooleanField(verbose_name=_('chocolate is available'), default=False)
    coffee_is_available = models.BooleanField(verbose_name=_('coffee is available'), default=False)
    muesli_is_available = models.BooleanField(verbose_name=_('muesli is available'), default=False)
    macarons_is_available = models.BooleanField(verbose_name=_('macarons is available'), default=False)
    cakes_is_available = models.BooleanField(verbose_name=_('cakes is available'), default=False)

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Availability Configuration')
        verbose_name_plural = _('Availability Configurations')


class IndexPageDescription(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        chocolate=models.CharField(max_length=200, verbose_name=_('chocolate description')),
        coffee=models.CharField(max_length=200, verbose_name=_('coffee description')),
        muesli=models.CharField(max_length=200, verbose_name=_('muesli description')),
        macarons=models.CharField(max_length=200, verbose_name=_('macarons description')),
        cake=models.CharField(max_length=200, verbose_name=_('cake description'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Index Page Product Description')
        verbose_name_plural = _('Index Page Product Descriptions')


class IndexPageTabsManagement(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        chocolate=models.TextField(verbose_name=_('chocolate tab text')),
        coffee=models.TextField(verbose_name=_('coffee tab text')),
        muesli=models.TextField(verbose_name=_('muesli tab text')),
        macarons=models.TextField(verbose_name=_('macarons tab text')),
        cake=models.TextField(verbose_name=_('cake tab text'))
    )
    chocolate_image = models.ImageField(verbose_name=_('chocolate image'))
    coffee_image = models.ImageField(verbose_name=_('coffee image'))
    muesli_image = models.ImageField(verbose_name=_('muesli image'))
    macarons_image = models.ImageField(verbose_name=_('macarons image'))
    cake_image = models.ImageField(verbose_name=_('cake image'))

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Index Page Tabs Management')
        verbose_name_plural = _('Index Page Tabs Management')


class IndexPageLeftBlockText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Index Page Left Block Text')
        verbose_name_plural = _('Index Page Left Block Text')


class ChocolateIndexPageText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Chocolate Index Page Text')
        verbose_name_plural = _('Chocolate Index Page Text')


class CoffeeIndexPageText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Coffee Index Page Text')
        verbose_name_plural = _('Coffee Index Page Text')


class MuesliIndexPageText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Muesli Index Page Text')
        verbose_name_plural = _('Muesli Index Page Text')


class MacaronsIndexPageText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Macarons Index Page Text')
        verbose_name_plural = _('Macarons Index Page Text')


class CakeIndexPageText(TranslatableModel, SingletonModel):
    translations = TranslatedFields(
        text=models.TextField(verbose_name=_('text'))
    )

    def __unicode__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = _('Cake Index Page Text')
        verbose_name_plural = _('Cake Index Page Text')


class Subscriber(models.Model):
    email = models.EmailField(verbose_name=_('email'))

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribers')
