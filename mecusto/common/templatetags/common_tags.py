from django import template
from django.http import QueryDict

register = template.Library()


@register.filter('replace')
def do_replace(value, args):
    """
    Replaces first given parameter with second in all value
    :param value: value to proceed with
    :param args: must be string in format "first=SMTHNG&second=SMTHNG2"
    :return: formatted  unicode value
    """
    qs = QueryDict(args)
    first, second = qs.get('first', ''), qs.get('second', '')
    return unicode(value).replace(first, second)


@register.filter('to_float')
def do_float(value):
    """
    Takes number and converts it to js-understandable float.
    If it has no value after decimal separator, it converts this number to integer
    :param value: number like 5,74 or 5,00
    :return: formatted unicode number
    """
    if value == int(value):
        return unicode(int(value))
    return unicode(value).replace(',', '.')


@register.assignment_tag
def get_flatpage(flatpage_queryset, **filters):
    return flatpage_queryset.get(**filters)
