from django.core.urlresolvers import reverse_lazy, resolve, Resolver404
from django.views.generic.base import TemplateView, RedirectView
from django.utils.translation import check_for_language, activate

from mecusto.utils.exceptions import ignored
from choco import models as choco_models
from coffee import models as coffee_models
from muesli import models as muesli_models
from cakes import models as cakes_models
from flatpages import models as flatpages_models
import models


class RootPage(RedirectView):
    permanent = True
    url = reverse_lazy('index-page')


class IndexPage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        kwargs.update({
            'products': models.IndexPageDescription.get_solo(),
            'left_block': models.IndexPageLeftBlockText.get_solo()
        })
        return kwargs


class SetLanguage(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        match, path, obj = None, 'index-page', None
        next_param = self.request.POST.get('next')

        if next_param:
            with ignored(Resolver404, KeyError):
                match = resolve(next_param.split('?')[0])

        if match:
            path = getattr(match, 'view_name')

        if 'lang' in self.request.POST:
            lang = self.request.POST.get('lang')
            if lang and check_for_language(lang):
                self.request.session['django_language'] = lang
                activate(lang)

                # ugly, nasty hack
                if match and match.kwargs:
                    if path == 'choco:constructor-page':
                        lst = choco_models.Chocolate.objects.language('all').filter(slug=match.kwargs['slug']).values('id')
                        obj = choco_models.Chocolate.objects.language().filter(id=lst[0]['id']).values('slug')[0]
                    elif path == 'coffee:constructor-page':
                        lst = coffee_models.Coffee.objects.language('all').filter(slug=match.kwargs['slug']).values('id')
                        obj = coffee_models.Coffee.objects.language().filter(id=lst[0]['id']).values('slug')[0]
                    elif path == 'muesli:constructor-page':
                        lst = muesli_models.Muesli.objects.language('all').filter(slug=match.kwargs['slug']).values('id')
                        obj = muesli_models.Muesli.objects.language().filter(id=lst[0]['id']).values('slug')[0]
                    elif path == 'cakes:constructor-page':
                        lst = cakes_models.Cake.objects.language('all').filter(slug=match.kwargs['slug']).values('id')
                        obj = cakes_models.Cake.objects.language().filter(id=lst[0]['id']).values('slug')[0]
                    elif path == 'flatpages:content-page':
                        lst = flatpages_models.Content.objects.language('all').filter(slug=match.kwargs['slug']).values('id')
                        obj = flatpages_models.Content.objects.language().filter(id=lst[0]['id']).values('slug')[0]

        if obj:
            self.url = reverse_lazy(path, kwargs=obj)
        else:
            self.url = reverse_lazy(path)

        if len(next_param.split('?')) == 2 and path != 'index-page':
            self.url += '?' + next_param.split('?')[1]
        return super(SetLanguage, self).get_redirect_url(*args, **kwargs)

root_page = RootPage.as_view()
index_page = IndexPage.as_view()
set_language = SetLanguage.as_view()
