#! -*- coding: utf-8 -*-
from django.contrib.admin.sites import AdminSite
from django.utils.translation import ugettext_lazy as _


class MecustoAdminSite(AdminSite):
    site_title = _('Mecusto Administrative Area')
    site_header = _('Mecusto Administrative Area')
    index_title = _('App Index')

mecusto_admin = MecustoAdminSite()
