from flatpages.models import Content
from choco.models import Chocolate


def get_content_pages(request):
    return {'flatpages': Content.objects.prefetch_related().all()}


def get_menu_items(request):
    return {
        'choco_menu_items': Chocolate.objects.all()
    }