import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

WEBSITE_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__))).replace('\\', '/')
PROJECT_ROOT = os.path.abspath(os.path.dirname(WEBSITE_ROOT)).replace('\\', '/')

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mecustodb',
        'USER': 'postgres',
        'PASSWORD': 'rwx2daxd',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}

ALLOWED_HOSTS = [
    '.mecusto.ee', '.megusto.ee'
]

TIME_ZONE = 'Europe/Kiev'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media_root')
MEDIA_URL = '/uploads/'

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static_root')
STATIC_URL = '/assets/'

# Additional locations of static files
STATICFILES_DIRS = (
   ('css', os.path.join(PROJECT_ROOT, 'css').replace('\\', '/')),
   ('js', os.path.join(PROJECT_ROOT, 'js').replace('\\', '/')),
   ('img', os.path.join(PROJECT_ROOT, 'img').replace('\\', '/')),
   ('ckeditor', os.path.join(PROJECT_ROOT, 'ckeditor').replace('\\', '/')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

SECRET_KEY = ')q=!b-i!53b$h%v^r^k#u@&*ck%t^q!b^juz_-i%jd1us-ow2+rq2+3oua'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'mecusto.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'pipeline.middleware.MinifyHTMLMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'mecusto.context_processors.common.get_content_pages',
    'mecusto.context_processors.common.get_menu_items',
)

ROOT_URLCONF = 'mecusto.urls'

WSGI_APPLICATION = 'mecusto.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(WEBSITE_ROOT, 'templates').replace('\\', '/'),
)

SESSION_COOKIE_NAME = 'mecustosid'

INSTALLED_APPS = (
    # django
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # external apps
    'pipeline',
    'rosetta',
    'adminsortable',
    'simplemathcaptcha',
    'hvad',
    'solo',
    'nested_inline',
    'ckeditor',
    # internal apps
    'common',
    'banner',
    'basket',
    'choco',
    'coffee',
    'muesli',
    'macarons',
    'cakes',
    'flatpages',
)


LANGUAGE_CODE = 'et'
LANGUAGE_COOKIE_NAME = 'lang'

from django.utils.translation import ugettext_lazy as _

LANGUAGES = (
    ('et', _('estonian')),
    ('ru', _('russian')),
)

LOCALE_PATHS = (os.path.join(WEBSITE_ROOT, 'locale').replace('\\', '/'),)

# The cache backends to use.
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211'
    }
}
CACHE_MIDDLEWARE_KEY_PREFIX = ''
CACHE_MIDDLEWARE_SECONDS = 600
CACHE_MIDDLEWARE_ALIAS = 'default'

# Rosetta Configuration
ROSETTA_MESSAGES_PER_PAGE = 25
# ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
# YANDEX_TRANSLATE_KEY = ''
ROSETTA_WSGI_AUTO_RELOAD = True
ROSETTA_EXCLUDED_APPLICATIONS = ()
ROSETTA_POFILE_WRAP_WIDTH = 78
ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'

# Pipeline Configuration
STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_YUGLIFY_BINARY = 'yuglify'
PIPELINE_COMPILERS = (
    'pipeline.compilers.coffee.CoffeeScriptCompiler',
    'pipeline.compilers.less.LessCompiler',
)
PIPELINE_COFFEE_SCRIPT_ARGUMENTS = '-m'
PIPELINE_LESS_ARGUMENTS = '-x'
PIPELINE_CSS = {
    'vendors': {
        'source_filenames': (
            'css/jquery-ui.min.css',
        ),
        'output_filename': 'css/vendors.min.css',
    },
    'style': {
        'source_filenames': (
            'css/style.less',
        ),
        'output_filename': 'css/style.css',
    },
}
PIPELINE_JS = {
    'vendors': {
        'source_filenames': (
            'js/vendor/jquery.slides.min.js',
            'js/vendor/jquery-ui.min.js',
            'js/vendor/underscore-min.js',
        ),
        'output_filename': 'js/vendors.min.js',
    },
    'common': {
        'source_filenames': (
            'js/coffee/utils.coffee',
            'js/coffee/products.coffee',
            'js/coffee/basket.coffee',
            'js/coffee/forms.coffee',
            'js/coffee/common.coffee',
        ),
        'output_filename': 'js/common.min.js',
    }
}
PIPELINE_DISABLE_WRAPPER = True

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'info@megusto.ee'
EMAIL_HOST_PASSWORD = 'k%"t30izl7'
EMAIL_PORT = 587

# CKEditor
CKEDITOR_UPLOAD_PATH = "site/"
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
    'content': {
        'skin': 'icy_orange',
        'toolbar': [
            ['Cut', 'Copy', 'Paste'],
            ['FontSize', 'Format'],
            ['Source', 'Undo', 'Redo'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', '-', 'Blockquote'],
            ['Link', 'Unlink', 'Anchor'],
            ['Image', 'Table', 'HorizontalRule', 'NumberedList', 'BulletedList', '-','SpecialChar', '-', 'CreateDiv']
        ],
        'height': 500,
        'width': 835,
    }
}
