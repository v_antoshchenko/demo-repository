from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.utils.translation import ugettext_lazy as _

from .admin import mecusto_admin
from .utils.i18n.urls import i18n_patterns


api_v1_patterns = patterns('',
    url(r'^/basket', include('basket.api.urls', namespace='basket')),
    url(r'^/common', include('common.api.urls', namespace='common')),
)

urlpatterns = patterns('common.views',
    url(r'^$', 'root_page', name='root-page'),
    url(r'^set-lang$', 'set_language', name='set-language'),
    url(r'^api/v1', include(api_v1_patterns, namespace='api-v1')),
)

# add slash. before url regex to separate it from language code
# only in these patterns
urlpatterns += i18n_patterns('',
    url(r'^$', 'common.views.index_page', name='index-page'),
    url(_(r'^/chocolate'), include('choco.urls', namespace='choco')),
    url(_(r'^/coffee'), include('coffee.urls', namespace='coffee')),
    url(_(r'^/muesli'), include('muesli.urls', namespace='muesli')),
    url(_(r'^/macarons'), include('macarons.urls', namespace='macarons')),
    url(_(r'^/cake'), include('cakes.urls', namespace='cakes')),
    url(_(r'^/basket'), include('basket.urls', namespace='basket')),
    url(r'', include('flatpages.urls', namespace='flatpages')),
)

# admin urls
urlpatterns += patterns('',
    url(r'^admin/rosetta/', include('rosetta.urls'), name='rosetta'),
    url(r'^admin/', include(mecusto_admin.urls)),
    url(r'^admin/ckeditor/', include('ckeditor.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
