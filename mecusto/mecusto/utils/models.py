from django.db import models
from django.utils.translation import ugettext_lazy as _


class ProductMixin(object):

    def __init__(self, *args, **kwargs):
        super(ProductMixin, self).__init__(*args, **kwargs)

    def __unicode__(self):
        """
        Default attribute is title
        :return: unicode representation
        """
        return self.safe_translation_getter('title', default=getattr(self, 'title'))

    def get_absolute_url(self):
        """
        Method for representing url path for object
        :return: full url path
        """
        return NotImplemented

    def get_price(self):
        """
        Method for representing price for object
        :return: floating point number
        """
        return NotImplemented


class AvailabilityMixin(models.Model):
    is_available = models.BooleanField(verbose_name=_('is available'), default=True)

    class Meta:
        abstract = True


class EnergyValueMixin(models.Model):
    energy_value = models.IntegerField(verbose_name=_('energy value'), default=0)
    carbohydrates = models.IntegerField(verbose_name=_('carbohydrates'), default=0)
    fats = models.IntegerField(verbose_name=_('fats'), default=0)

    class Meta:
        abstract = True


class PositionMixin(models.Model):
    position = models.PositiveIntegerField(verbose_name=_('position'), default=0)

    class Meta:
        abstract = True
